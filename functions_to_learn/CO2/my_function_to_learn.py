#!bin/python
import os
import numpy as np
import subprocess
import uuid
from jinja2 import Template
import yaml
import math

from constants import *


def my_function_to_learn(xy, id1): #MOLPRO ND
    data_base_folder=str(os.getcwd())+"/"+"computed_points/"
    if not os.path.exists(data_base_folder):
      os.mkdir(data_base_folder)
    geometry = xy
    exit_status=os.system("./a.out" + ' ' +  str(geometry[0]) + ' ' +  str(geometry[1]) + ' ' +  str(geometry[2]) + ' ' +' >>/dev/null')
    if exit_status!=0:
      print("The fortran command failed")
      exit()
    else:
      #Once this is over, get what you are interested in from the output
      proc = subprocess.Popen("./a.out" + ' ' + str(geometry[0]) + ' ' +  str(geometry[1]) + ' ' +  str(geometry[2]) + ''' | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
      #proc = subprocess.Popen("awk '/SETTING ESCF/{print $4;exit}' "+str(directory_path)+'''hono.out''', stdout=subprocess.PIPE, shell=True)
      energy=proc.communicate()[0]
      energy=str((energy.decode('utf-8')).strip()).replace("D", "E")
      #Keep it somewhere

      path_file_db_mapped=data_base_folder+"hfco_tab_mapped.txt"
      newline=''
      for i in range(len(geometry)):
        newline+="{:.8f}".format(geometry[i])+' '
      newline+="{}".format(float(energy))+' '
      print(newline,file=open(path_file_db_mapped,"a"))

    return (float(energy), id1)
