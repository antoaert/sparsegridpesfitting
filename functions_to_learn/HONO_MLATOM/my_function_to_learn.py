#!bin/python
import os
import numpy as np
import subprocess
import uuid
from jinja2 import Template
import yaml
import math

from constants import *

def base_model(xy):
    pot=1.
    return pot
    


def my_function_to_learn(xy, id1): #MOLPRO ND
    data_base_folder=str(os.getcwd())+"/"+"computed_points/"
    if not os.path.exists(data_base_folder):
      os.mkdir(data_base_folder)
    geometry = xy
    #The procedure will provide the lengths in Bohr, molpro will use them in angstrom
    geometry_mapped=np.zeros(len(geometry))
    geometry_mapped[0]=geometry[0]/angstrom_to_bohr
    geometry_mapped[1]=geometry[1]/angstrom_to_bohr
    geometry_mapped[2]=geometry[2]/angstrom_to_bohr
    geometry_mapped[3]=np.arccos(geometry[3])/radian_per_degree
    geometry_mapped[4]=np.arccos(geometry[4])/radian_per_degree
    geometry_mapped[5]=geometry[5]/radian_per_degree
    template_zmat='''
    N 
    O1 1 {{r_o1n}} 
    O2 1 {{r_o2n}} 2 {{a_noo}} 
    H 3 {{r_o2h}} 1 {{a_hon}} 2 {{d_hono}}    
    '''    

    #Use this list that define the geometry in the coordinates (and units) of the z-matrix in a template of the molpro calculation. 
    tm=Template(template_zmat)

    zmat_file=tm.render(
    r_o1n=geometry_mapped[0],
    r_o2n=geometry_mapped[1],
    r_o2h=geometry_mapped[2],
    a_noo=geometry_mapped[3],
    a_hon=geometry_mapped[4],
    d_hono=geometry_mapped[5])

    unique_filename = str(uuid.uuid4())
    directory_path=str(os.getcwd())+"/mlatom_computations/"+"hono"+unique_filename+"/"
    directory_path1=str(os.getcwd())+"/mlatom_computations"

    if not os.path.exists(directory_path1):
        os.mkdir(directory_path1)
    
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)
    zmat_file_path=directory_path+"hono"+".zmat"
    with open(zmat_file_path, 'w') as f:
         print(zmat_file, file=f)
    f.close()

    template_mlatom='''
    nthreads=2
    AIQM1
    yestfile=ene.dat
    multiplicities=1
    xyzfile=input.xyz'''
 
    input_file_path=directory_path+"hono"+".inp"
    with open(input_file_path, 'w') as f:
         print(template_mlatom, file=f)
    f.close()

    #Once the input file is created, run the program
    exit_status=os.system("cd "+str(directory_path) + " && " + "/opt/GeomConvert/geomConvert/gc.py -zmat hono.zmat | sed 's/.//2' > input.xyz && /home/antoaert/myDockers/MLAtom/mlatom hono.inp")  #+str(input_file_path))
 
    if exit_status!=0:
        print("The mlatom command failed")
        exit()
    else:
        #Once this is over, get what you are interested in from the output
        proc = subprocess.Popen('cat '+str(directory_path)+'''ene.dat | tr -s ' ' | cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
        energy=proc.communicate()[0]
        #Keep it somewhere
        path_file_db=data_base_folder+"hono_tab.txt"
        newline=''  #str(current_term).replace(' ','')+' '
        for i in range(len(geometry_mapped)):
          newline+="{:.8f}".format(geometry[i])+' '
        newline+="{}".format(float(energy))+' '
        print(newline,file=open(path_file_db,"a"))

    return (float(energy), id1)
