#!bin/python
import os
import numpy as np
import subprocess
import uuid
from jinja2 import Template
import yaml

from constants import *

def my_function_to_learn(xy, id1): #MOLPRO ND
    data_base_folder=str(os.getcwd())+"/"+"computed_points/"
    if not os.path.exists(data_base_folder):
      os.mkdir(data_base_folder)
    geometry = xy
    #The procedure will provide the lengths in Bohr, molpro will use them in angstrom
    geometry_mapped=np.zeros(len(geometry))
    geometry_mapped[0]=geometry[0]/angstrom_to_bohr
    geometry_mapped[1]=geometry[1]/angstrom_to_bohr
    geometry_mapped[2]=geometry[2]/angstrom_to_bohr
    geometry_mapped[3]=np.arccos(geometry[3])/radian_per_degree
    geometry_mapped[4]=np.arccos(geometry[4])/radian_per_degree
    geometry_mapped[5]=geometry[5]/radian_per_degree

    #Use this list that define the geometry in the coordinates (and units) of the z-matrix in a template of the molpro calculation. 
    template_molpro_hono='''***, HONO aug-cc-pVTZ-F12 uks
    memory,250,m   
    print orbitals
    geometry={angstrom
    N 
    O 1 RO1N  
    O 1 RO2N 2 ANOO 
    H 3 RO2H 1 AHON 2 D}
   
    !basis=cc-pVTZ
    basis=cc-pVTZ-f12
    RO1N={{r_o1n}}
    RO2N={{r_o2n}}
    RO2H={{r_o2h}}
    ANOO={{a_noo}}
    AHON={{a_hon}}
    D={{d_hono}}
    hf;
    escf=energy
    ccsd(t)-f12;
    eccsdt=energy(1)
    !eccsdtb=energy(2)
    '''
    tm=Template(template_molpro_hono)

    input_file=tm.render(
    r_o1n=geometry_mapped[0],
    r_o2n=geometry_mapped[1],
    r_o2h=geometry_mapped[2],
    a_noo=geometry_mapped[3],
    a_hon=geometry_mapped[4],
    d_hono=geometry_mapped[5])

    unique_filename = str(uuid.uuid4())
    directory_path=str(os.getcwd())+"/molpro_computations/"+"hono"+unique_filename+"/"
    directory_path1=str(os.getcwd())+"/molpro_computations"
    if not os.path.exists(directory_path1):
        os.mkdir(directory_path1)
    
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)

    input_file_path=directory_path+"hono"+".inp"
    with open(input_file_path, 'w') as f:
         print(input_file, file=f)
    f.close()
    #Once the input file is created, run the program
    exit_status=os.system("cd "+str(directory_path)+ " && " + "/opt/Molpro/2024.1.0/molpro_2024.1/bin/molpro -n 4 hono.inp")  #+str(input_file_path))
  
    if exit_status!=0:
        print("The molpro command failed")
        exit()
    else:
        #Once this is over, get what you are interested in from the output
        proc = subprocess.Popen('grep -r "SETTING ECCSDT" '+str(directory_path)+'''hono.out | head -1 | cut -d "=" -f 2 | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
        #proc = subprocess.Popen("awk '/SETTING ESCF/{print $4;exit}' "+str(directory_path)+'''hono.out''', stdout=subprocess.PIPE, shell=True)
        energy=proc.communicate()[0]
        energy1=energy
        #Keep it somewhere
        path_file_db=data_base_folder+"hono_tab.txt"
        newline=''  #str(current_term).replace(' ','')+' '
        for i in range(len(geometry_mapped)):
          newline+="{:.8f}".format(geometry[i])+' '
        newline+="{}".format(float(energy))+' '
        print(newline,file=open(path_file_db,"a"))
 

    return (float(energy), id1)
