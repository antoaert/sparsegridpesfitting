"""
=====================
Demo of 3D bar charts
=====================

A basic demo of how to plot 3D bars with and without shading.
"""

import matplotlib.pyplot as plt
import numpy as np
import sys
import yaml
import ast

filename_expansion = sys.argv[1]

index_1 = int(sys.argv[2])
index_2 = int(sys.argv[3])
index_3 = int(sys.argv[4])

expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)

active_index_set = expansion['ActiveIndexSet']
old_index_set = expansion['OldIndexSet']

_x = []
_y = []
top = []
for i in range(len(active_index_set)):
    _x.append(active_index_set[i][index_1])
    _y.append(active_index_set[i][index_2])
    top.append(active_index_set[i][index_3])
for i in range(len(old_index_set)):
    _x.append(old_index_set[i][index_1])
    _y.append(old_index_set[i][index_2])
    top.append(old_index_set[i][index_3])

fig = plt.figure(figsize=(12, 6))
ax1 = fig.add_subplot(121, projection='3d')

# fake data
#_x = np.arange(4)
#_y = np.arange(5)
_x = np.array(_x)
_y = np.array(_y)
_xx, _yy = np.meshgrid(_x, _y)
x, y = _xx.ravel(), _yy.ravel()

#top = x + y
top = np.array(top)
bottom = np.zeros_like(top)
width = depth = 1

#dx = np.ones(10)
#dy = np.ones(10)
#dz = np.ones(10)

#ax1.bar3d(_x, top, _y, dx, dy, dz)

ax1.bar3d(_x, _y, bottom, width, depth, top, shade=True)

#ax1.set_title('Shaded')

#ax2.bar3d(x, y, bottom, width, depth, top, shade=False)
#ax2.set_title('Not Shaded')

plt.show()
