import sys
import yaml
import ast
import numpy as np
import warnings

filename_expansion = sys.argv[1]
restartname_expansion = sys.argv[2]


extrapol = True
additional_string = 'CI'
dimension_of_step = [4]

expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
restart = yaml.load(open(restartname_expansion, 'r'), Loader=yaml.Loader)

boundaries_coordinates=restart['internal_limits']

i=0
warned = False
labels_pol = []
labels_step = []
parameters = []
hamiltonian = []
d_cuts = [ [] for _ in range(len(boundaries_coordinates)) ] 

if extrapol == True:
    keys_list = []
    for key in expansion:
        level_index, point_index = ast.literal_eval(key.replace('][', '],['))
        keys_list.append([level_index, point_index])
    array_keys = np.array(keys_list)
#print(np.unique(array_keys[:,0,0])) #all level indexes of dimension 0
#print(np.unique(array_keys[:,0,5])) #all level indexes of dimension 5
#print(array_keys[array_keys[:,0,5] == 7]) #Array elements where the dimension 5 level index is 7

#for dim in range(len(boundaries_coordinates)):
#    unique_level_indexes = np.unique(array_keys[:,0,dim])
#    for i in unique_level_indexes:
#        selected_elements = array_keys[array_keys[:,0,dim] == i] 
#        print(dim,i,np.amax(selected_elements[:,1,dim])) #maximum point index for level index i in dimension dim
#        print(dim,i,np.amin(selected_elements[:,1,dim]))


for key in expansion:
#    print(key, expansion[key])
    string_parameter=''
    string_hamil=''
    level_index, point_index = ast.literal_eval(key.replace('][', '],['))
    level_type = 0
    for index in level_index:
        if index > 1:
            level_type += 1

    surplus = expansion[key][0]
#    print(surplus, str('{}'.format(surplus).replace('e', 'd')))
    if surplus != 0.0:
#    print('func', level_index, point_index)

        if level_index == [1]*len(level_index):  #The very first point is a special case since it is just a constant
            string_parameter = 'P0=0.0d0'  +'#' + str('{}'.format(surplus).replace('e', 'd'))
            parameters.append(string_parameter)
            string_hamil = 'P0' + '   ' + ' |  1'*len(level_index)
            hamiltonian.append(string_hamil)
        else:
            string_surpluses = ''
            for dim in range(len(level_index)):
                if extrapol == True:
                    selected_elements = array_keys[array_keys[:,0,dim] == level_index[dim]] 
                    max_pointindex = np.amax(selected_elements[:,1,dim]) #maximum point index for level index i in dimension dim
                    min_pointindex = np.amin(selected_elements[:,1,dim])

                factor_scaling = (boundaries_coordinates[dim][1]-boundaries_coordinates[dim][0])
                shifting_scaling = boundaries_coordinates[dim][0] 
                factor_scaling_spacing = (boundaries_coordinates[dim][1]-boundaries_coordinates[dim][0]) 
                l = level_index[dim]
                j = point_index[dim]
                if l != 1:
                    string_surpluses += '' + str(dim+1)
                    string_surpluses += '' + str(l) + 'P' + str(j)
                spacing = 2.0**(-1.*l)
                spacing_scaled = spacing*factor_scaling_spacing
                position_point = j*spacing
                position_point_scaled = position_point*factor_scaling+shifting_scaling
                newlabel_pol = additional_string+'PD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=q['+str(position_point_scaled)+']'
                if extrapol == True:
                    if j == int(max_pointindex):
                        newlabel_step = additional_string+'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=step['+str(shifting_scaling+(j-1)*spacing_scaled)+']' 
                    elif j == int(min_pointindex):
                        newlabel_step = additional_string+'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=rstep['+str(shifting_scaling+(j+1)*spacing_scaled)+']' 
                    else: 
                        newlabel_step = additional_string+'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=charfun['+str(shifting_scaling+(j-1)*spacing_scaled)+','+str(shifting_scaling+(j+1)*spacing_scaled)+']' 
                elif extrapol == False:
                    if j+1 == int(2**l): 
                        newlabel_step = additional_string+'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=step['+str(shifting_scaling+(j-1)*spacing_scaled)+']' 
                    elif j-1 == 0: 
                        newlabel_step = additional_string+'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=rstep['+str(shifting_scaling+(j+1)*spacing_scaled)+']' 
                    else: 
                        newlabel_step =additional_string+ 'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=charfun['+str(shifting_scaling+(j-1)*spacing_scaled)+','+str(shifting_scaling+(j+1)*spacing_scaled)+']' 
                string_step = additional_string+'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)
                if not(newlabel_pol in labels_pol):
                    labels_pol.append(newlabel_pol)
                if not(newlabel_step in labels_step):
                    labels_step.append(newlabel_step)
        #       To obtain the nd function, we call basisnd(np.array(*args), level_index , point_index)
        #       It is the product of each 1d function accross the dimensions
                if l != 1:
                    if level_type == 1: dim1 = dim
                    # We need the ancestors of this point
                    ancestors = [0.5]  #If want bounds --> add them here!!
                    for h in range(2, l):
                        if position_point > ancestors[-1]:
                            ancestors.append(ancestors[-1]+2.0**(-1.*h))
                        elif position_point < ancestors[-1]:
                            ancestors.append(ancestors[-1]-2.0**(-1.*h))
                    ancestors = np.array(ancestors)  # Contains the position of the ancestors accross the 1D projection
                    ancestors_scaled = np.array(ancestors)*factor_scaling+shifting_scaling
                    coeff = np.prod(1./(position_point_scaled-ancestors_scaled))
                    surplus = surplus*coeff
                    #Need to figure out what are the names of the ancestor polynomials
                    ancestor_level = 1
                    string_hamil += '|' + str(dim+1) + '   ' 
                    for point in ancestors:
                        string_hamil += additional_string+'PD' + str(dim+1) + 'L' + str(ancestor_level) + 'P' + str(int(point/(2.0**(-1.*ancestor_level)))) + '*'
                        ancestor_level += 1 
                    if dim+1 in dimension_of_step:
                        if warned == False:
                            warnings.warn(f'The function SW{str(dim+1)+additional_string}  has to be define by the user')
                            warned = True
                        string_hamil += string_step + '*SW'+str(dim+1)+additional_string+' '
                        if level_index[dim] == 1:
                            string_hamil += '|'+str(dim+1)+' SW'+str(dim+1)+additional_string+' '
                    else:
                        string_hamil += string_step + '   '
            string_hamil = str('{:.8e}'.format(surplus).replace('e', 'd')) + '   ' + string_hamil
            if level_type == 1: d_cuts[dim1].append(string_hamil) 
            hamiltonian.append(string_hamil)
             
    i += 1



op_1='''OP_DEFINE-SECTION
title
HFCO in valence coordinates. r1=r(C-H), r2=r(C-F), r3=r(C-O), beta1=(O-C-H), beta2=(O-C-F)
phi = angle (O-C-F) and (O-C-H).
end-title
end-op_define-section

PARAMETER-SECTION'''

print(op_1)

for j in parameters: 
    print(j)

op_2='''end-parameter-section
LABELS-SECTION'''

print(op_2)


for k in labels_pol:
    print(k)

for k in labels_step:
    print(k)

op_3='''end-labels-section
HAMILTONIAN-SECTION
-----------------------------------'''

print(op_3)
label_modes = 'modes   '
for j in range(len(level_index)):
    label_modes += '| ' + 'x_' + str(j)
print(label_modes)

print('-----------------------------------')

for k in hamiltonian:
    print(k)
op_4='''-----------------------------------
end-hamiltonian-section
'''
print(op_4)

for j in range(len(level_index)):
    print('hamiltonian-section_cut.' + str(j+1))
    print('-----------------------------------')
    print(label_modes)
    print('-----------------------------------')
    for k in d_cuts[j]:
        print(k)
    print('end-hamiltonian-section')




print('end-operator')
#print(hamiltonian)

print('This dataset contains ', i, ' points')

