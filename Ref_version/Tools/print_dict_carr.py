import sys
import yaml
import ast
import numpy as np

filename_model = sys.argv[1] 
filename_expansion = sys.argv[2]
restartname_expansion = sys.argv[3]

model     = yaml.load(open(filename_model, 'r'),        Loader = yaml.Loader)
expansion = yaml.load(open(filename_expansion, 'r'),    Loader = yaml.Loader)
restart   = yaml.load(open(restartname_expansion, 'r'), Loader = yaml.Loader)

boundaries_coordinates=restart['internal_limits']
b_type = restart['b_type']
alpha = restart['alpha']
minimum_coordinates = restart['minimum_coordinates']
dimension = len(b_type)

def express_function(b_type, alpha, x_ref, k):
    str_x_ref = str('{:.8e}'.format(x_ref).replace('e', 'd'))
    str_alpha = str('{:.8e}'.format(alpha).replace('e', 'd'))
    if b_type == 'polynomial':
        express = 'q['+str_x_ref+']'
    elif b_type == 'morse':
        express = 'exp1[-'+str_alpha+','+str_x_ref+']'
    elif b_type == 'trigonometric':
        if k % 2 != 0:
            express = 'sin[' + str(int((k+2)/2)) + ',' + str_x_ref + ']'
        else:
            express = 'sin[' + str(0.5*int((k+2)/2)) + ',' + str_x_ref + ']^2'
    return express


i=0
labels_pol = []
labels_step = []
parameters = []
hamiltonian = []
d_cuts = [ [] for _ in range(dimension) ] 


for key in model:
    string_parameter=''
    string_hamil=''
    term, exponent_orders, point_index_in_1D_cut = ast.literal_eval(key.replace('][', '],['))
    level_type = sum(term)

    surplus = model[key]
    if surplus != 0.0:
        if term == [0]*dimension:  #The very first point is a special case since it is just a constant
            string_parameter = 'P0=0.0d0'  # + str('{}'.format(surplus).replace('e', 'd'))
            parameters.append(string_parameter)
            string_hamil = 'P0' + '   ' + ' |  1'*dimension
            hamiltonian.append(string_hamil)
        else:
            surplus = model[key]
            multiplier = 1.
            t=0
            for dim in range(dimension):
                if term[dim] == 1:
                    j = exponent_orders[t] #point_index_in_1D_cut[t]
                    exp = exponent_orders[t]
                    if level_type == 1:
                        dim1 = dim
#                         newlabel_pol = 'PD'+str(dim+1)+'L'+str(j)+'='+express_function(b_type[dim], alpha[dim],minimum_coordinates[dim]) 
                    if b_type[dim] == 'trigonometric':
                        multiplier = 2.
                        newlabel_pol = 'PD'+str(dim+1)+'L'+str(j) + '='+express_function(b_type[dim], alpha[dim],minimum_coordinates[dim], j) 
                        label_func = 'PD'+str(dim+1)+'L'+str(j) + '    '
                    else:
                        newlabel_pol = 'PD'+str(dim+1)+'='+express_function(b_type[dim], alpha[dim],minimum_coordinates[dim], j) 
                        label_func = 'PD'+str(dim+1) + '^' + str(j+1) + '    '
                    if not(newlabel_pol in labels_pol):
                        labels_pol.append(newlabel_pol)
                    string_hamil += '|' + str(dim+1) + '   ' 
                    string_hamil +=  label_func
                    t += 1
            string_hamil = str('{:.8e}'.format(surplus*multiplier).replace('e', 'd')) + '   ' + string_hamil
            if level_type == 1: d_cuts[dim1].append(string_hamil) 
            hamiltonian.append(string_hamil)
             
    i += 1



op_1='''OP_DEFINE-SECTION
title
HONO 
end-title
end-op_define-section

PARAMETER-SECTION'''

print(op_1)

for j in parameters: 
    print(j)

op_2='''q20  = 2.696732586
q30  = 1.822912197
q10  = 2.213326419
q11  = 1.8653
th20 = 1.777642018
th10 = 1.9315017
mh   = 1.0, H-mass
mc   = 12.0,AMU
mo   = 15.9949,AMU
mn   = 13.9939,AMU
M11  = 1.0/mo+1.0/mh
M22  = 1.0/mo+1.0/mn
M33  = M22
M13  = 1.0/mo
M23  = -1.0/mn
p1   = PI/2.0
p2   = 3.0*PI/2.0
end-parameter-section
LABELS-SECTION'''

print(op_2)


for k in labels_pol:
    print(k)

for k in labels_step:
    print(k)

op_3='''g0pi = gauss[2.0,0.0]
g1pi = gauss[2.0,PI]
g2pi = gauss[2.0,2.0*PI]
qs1  = qs[1.0]
end-labels-section
HAMILTONIAN-SECTION
-----------------------------------'''

print(op_3)
label_modes = 'modes   '
for j in range(dimension):
    label_modes += '| ' + 'x_' + str(j)
print(label_modes)

print('''-----------------------------------
-M11/2.0  | 1      |   1    |  dq^2   |   1         |    1         | 1
-M22/2.0  | dq^2   |   1    |   1     |   1         |    1         | 1
-M33/2.0  | 1      |   dq^2 |   1     |   1         |    1         | 1
-M13      | 1      |   dq   |   dq    |   1         |    q         | 1
 M23      | dq     |   dq   |   1     |   q         |    1         | 1
-M13      | 1      |   q^-1 |   q^-1  |   1         |    q         | 1
 M23      | q^-1   |   q^-1 |   1     |   q         |    1         | 1

-M13      |  1      |  q^-1 |   dq    |   1         |  udq2        | 1
 M13      |  1      |  q^-1 |   dq    |  udq        |    qs1       | cos
-M23      |  dq     |  q^-1 |   1     |  qs1        |  udq         | cos
 M23      |  dq     |  q^-1 |   1     |  udq2       |    1         | 1
-M13      |  1      |  dq   |   q^-1  |   1         |  udq2        | 1
 M23      |  q^-1   |  dq   |   1     |  udq2       |    1         | 1

 M13      | 1       | q^-1  |   dq    |  q*qs1^-1   |   qs1        | sdq
-M23      | dq      | q^-1  |   1     |  qs1        |   q*qs1^-1   | sdq

-M11/2.0  | 1       |  1    |   q^-2  |     1       | dq*qs1^2*dq  | 1
-M33/2.0  | 1       |  q^-2 |    1    |     1       | dq*qs1^2*dq  | 1
 M13      | 1       |  q^-1 |   q^-1  |     1      | dq*qs1^2*q*dq | 1
-M22/2.0  | q^-2    |  1    |   1     | dq*qs1^2*dq |     1        | 1
-M33/2.0  | 1       |  q^-2 |   1     | dq*qs1^2*dq |     1        | 1
-M23      | q^-1    |  q^-1 |   1     | dq*qs1^2*q*dq |   1        | 1

-M13/2.0  | 1       |  q^-1 |   q^-1  | qs1*dq      | dq*q*qs1     | cos
-M13/2.0  | 1       |  q^-1 |   q^-1  | dq*qs1      | q*qs1*dq     | cos
 M23/2.0  | q^-1    |  q^-1 |    1    | q*qs1*dq    | dq*qs1       | cos
 M23/2.0  | q^-1    |  q^-1 |    1    | dq*q*qs1    | qs1*dq       | cos
 M33/2.0  |  1      |  q^-2 |    1    | qs1*dq      | dq*qs1       | cos
 M33/2.0  |  1      |  q^-2 |    1    | dq*qs1      | qs1*dq       | cos

-M13/2.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | dq*q*qs1     | sdq
-M13/2.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | q*qs1*dq     | sdq
-M13/4.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | q^2*qs1^-1   | cos
 M13/4.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | qs1          | cos
 M23      |  q^-1   |  q^-1 |    1    | qs1^-1      | udq          | sdq
-0.25*M23 |  q^-1   |  q^-1 |    1    |  qs1^-1     |   q*qs1^-1   | cos

 M33      |  1      |  q^-2 |    1    | q*qs1^-1    | udq          | sdq

 M23/2.0  |  q^-1   |  q^-1 |    1    | dq*q*qs1    | q*qs1^-1     | sdq
 M23/2.0  |  q^-1   |  q^-1 |    1    | q*qs1*dq    | q*qs1^-1     | sdq
 M23/4.0  |  q^-1   |  q^-1 |    1    | q^2*qs1^-1  | q*qs1^-1     | cos
-M23/4.0  |  q^-1   |  q^-1 |    1    | qs1         | q*qs1^-1     | cos
-M13      |  1      |  q^-1 |   q^-1  | udq         | qs1^-1       | sdq
-M13/4.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | qs1^-1       | cos

 M33      |  1      |  q^-2 |    1    | udq         | q*qs1^-1     | sdq

-M11/2.0  |  1      |  1    |   q^-2  |    1        |   qs1^-2     | dq^2
 M13/2.0  |  1      |  q^-1 |   q^-1  |  q*qs1^-1   |   qs1^-1     | cos*dq^2
 M13/2.0  |  1      |  q^-1 |   q^-1  |  q*qs1^-1   |   qs1^-1     | dq^2*cos
 M13/2.0  |  1      |  q^-1 |   q^-1  |  q*qs1^-1   |   qs1^-1     | cos
 M13      |  1      |  q^-1 |   q^-1  |    1        |   q*qs1^-2   | dq^2

-M22/2.0  |  q^-2   |  1    |    1    |  qs1^-2     |     1        | dq^2
-M23/2.0  |  q^-1   |  q^-1 |    1    |  qs1^-1     |   q*qs1^-1   | dq^2*cos
-M23/2.0  |  q^-1   |  q^-1 |    1    |  qs1^-1     |   q*qs1^-1   | cos*dq^2
-M23      |  q^-1   |  q^-1 |    1    |  q*qs1^-2   |     1        | dq^2
-M33/2.0  |  1      |  q^-2 |    1    |    1        | q^2*qs1^-2   | dq^2
-M33/2.0  |  1      |  q^-2 |    1    |  q^2*qs1^-2 |     1        | dq^2
-M33/2.0  |  1      |  q^-2 |    1    |  q*qs1^-1   |   q*qs1^-1   | dq^2*cos
-M33/2.0  |  1      |  q^-2 |    1    |  q*qs1^-1   |   q*qs1^-1   | cos*dq^2
''')

for k in hamiltonian:
    print(k)
op_4='''-----------------------------------
end-hamiltonian-section
'''
print(op_4)

cut_string=[''' 5.862318d-6   | q^-1    |  1      | 1       | 1        | 1          | 1
-3.674923d-5   | dq^2    |  1      | 1       | 1        | 1          | 1''',
'''
 1.152891d-5   | 1       | q^-1    | 1       | 1        | 1          | 1
-3.6749237d-5  | 1       | dq^2    | 1       | 1        | 1          | 1
''',
'''
 3.243973d-6   | 1       |  1      | q^-1    | 1        | 1          | 1
-2.893093d-4   | 1       |  1      | dq^2    | 1        | 1          | 1
''',
'''
-6.56776d-6      |  1      |  1      |  1   | q             |   1      | 1
-1.25549d-5      |  1      |  1      |  1   | dq*qs1^2*dq   |   1      | 1
 6.56776d-6      |  1      |  1      |  1   | dq*q*qs1^2*dq |   1      | 1
''',
'''
-7.07777d-6    | 1       |  1      | 1       | 1      |  q            | 1
-9.06079d-5    | 1       |  1      | 1       | 1      | dq*qs1^2*dq   | 1
 7.07777d-6    | 1       |  1      | 1       | 1      | dq*q*qs1^2*dq | 1
''',
'''
-1.14605d-4    | 1       |  1      | 1       | 1        | 1       | dq^2
-3.07824d-6    | 1       |  1      | 1       | 1        | 1       | dq^2*cos
-3.07824d-6    | 1       |  1      | 1       | 1        | 1       | cos*dq^2
0.005 |6 g0pi
0.005 |6 g2pi
''',
'''
-1.14605d-4    | 1       |  1      | 1       | 1        | 1       | dq^2
-3.07824d-6    | 1       |  1      | 1       | 1        | 1       | dq^2*cos
-3.07824d-6    | 1       |  1      | 1       | 1        | 1       | cos*dq^2
0.006 |6 g1pi
''']


for j in range(dimension):
    print('hamiltonian-section_cut.' + str(j+1))
    print('-----------------------------------')
    print(label_modes)
    print('-----------------------------------')
    print(cut_string[j])
    for k in d_cuts[j]:
        print(k)
    print('end-hamiltonian-section')
print('hamiltonian-section_cut.6trans')
print('-----------------------------------')
print(label_modes)
print('-----------------------------------')
print(cut_string[5])
for k in d_cuts[-1]:
    print(k)
print('end-hamiltonian-section')
print('hamiltonian-section_cut.6cis')
print('-----------------------------------')
print(label_modes)
print('-----------------------------------')
print(cut_string[6])
for k in d_cuts[-1]:
    print(k)
print('end-hamiltonian-section')




print('end-operator')
#print(hamiltonian)

print('The potential part of the Hamiltonian contains ', i, ' coefficients')

