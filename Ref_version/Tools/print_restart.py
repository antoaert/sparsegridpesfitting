import sys
import yaml
import ast
import numpy as np

filename_expansion = sys.argv[1]


expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)


for key in expansion:
    print(key, expansion[key])
