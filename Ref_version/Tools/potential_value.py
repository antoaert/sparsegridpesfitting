import sys
sys.path.append('Source/')
import yaml

from basisnd import potential_nd
 
filename_expansion = sys.argv[1]
restartname_expansion = sys.argv[2]

expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
restart = yaml.load(open(restartname_expansion, 'r'), Loader=yaml.Loader)

boundaries_coordinates=restart['boundaries']
internal_limits = restart['internal_limits']


#Generate points and ask for value of the potential at that point:
#Boundaries = (1.90,2.60),(2.1,3.25),(1.3,2.45),(-0.65,-0.1),(-0.65,0.25),(0,3.14)
point = [2.25, 2.675, 1.875, -0.375, -0.2, 1.57]

print(potential_nd(point, expansion,internal_limits, boundaries_coordinates))



