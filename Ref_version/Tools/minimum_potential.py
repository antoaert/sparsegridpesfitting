import sys
sys.path.append('Source/')
import yaml
from scipy.optimize import fmin
from functools import partial

from basisnd import potential_nd
from basisnd import potential_nd_func
 
filename_expansion = sys.argv[1]
restartname_expansion = sys.argv[2]

expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
restart = yaml.load(open(restartname_expansion, 'r'), Loader=yaml.Loader)

boundaries_coordinates = restart['boundaries']
internal_limits = restart['internal_limits']

guess_minimum = [2.53889300,2.26805618,2.06728337,1.82888584 ,2.17930733,1.91994910,1.86301218,-3.14159265,0.0000000]

pot_func = potential_nd_func(expansion)

#minimum = fmin(pot_func, guess_minimum)
minimum = fmin(partial(potential_nd, surplus_dict=expansion, internal_limits=internal_limits, boundaries_coordinates=boundaries_coordinates), guess_minimum)

print(list(minimum))

