import sys
sys.path.append('Source/')
import yaml
from scipy.optimize import fmin
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import math


from basisnd import potential_nd_func, evaluate_model
from basisnd import position_processing_descale

filename_expansion = sys.argv[1]
restartname_expansion = sys.argv[2]

data_points = sys.argv[3]

COORDINATE = int(sys.argv[4])

expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
restart = yaml.load(open(restartname_expansion, 'r'), Loader=yaml.Loader)

data = np.loadtxt(data_points)

boundaries_coordinates = restart['boundaries']
internal_limits = restart['internal_limits']
LF = restart['LF'] #Lagrange Flag
if LF:
    b_type = restart['b_type']
minimum_coordinates = restart['minimum_coordinates']

minimum=minimum_coordinates


#GENERATE A 1D TERM
batchsize=100 #number of points to plot
#COORDINATE=0 #int that defines the coordinate that you want to plot


x_plot_1D=np.sort(np.random.uniform(low=boundaries_coordinates[COORDINATE][0], high=boundaries_coordinates[COORDINATE][1],size=batchsize))
#x_plot_1D=np.sort(np.random.uniform(low=1.97, high=2.70,size=batchsize))

y_out=np.zeros(batchsize)
t=0
pot_func = potential_nd_func(expansion)

for point1d in x_plot_1D:
  point=np.zeros(len(minimum))
  for j in range(len(minimum)):
    if j == COORDINATE:
      point[j] = point1d
#    elif j == 2:
#      point[j] = 3.6 
    else:
      point[j] = minimum[j]
  if not(LF):
    point = position_processing_descale(point, internal_limits, boundaries_coordinates)
    y_out[t]=pot_func(point)
  else:
    y_out[t]=evaluate_model(point, expansion, restart)#pot_func(pointa)
  t+=1
plt.plot(x_plot_1D,y_out)#*219474.)
plt.plot(data[:, COORDINATE], data[:, -1], '+')
plt.show()
