import sys
import yaml
import ast
import numpy as np

filename_expansion = sys.argv[1]
restartname_expansion = sys.argv[2]

expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
restart = yaml.load(open(restartname_expansion, 'r'), Loader=yaml.Loader)

boundaries_coordinates=restart['internal_limits']

i=0
labels_pol = []
labels_step = []
parameters = []
hamiltonian = []
d_cuts = [ [] for _ in range(len(boundaries_coordinates)) ] 
for key in expansion:
#    print(key, expansion[key])
    string_parameter=''
    string_hamil=''
    level_index, point_index = ast.literal_eval(key.replace('][', '],['))
    level_type = 0
    for index in level_index:
        if index > 1:
            level_type += 1

    surplus = expansion[key][0]
#    print(surplus, str('{}'.format(surplus).replace('e', 'd')))
    if surplus != 0.0:
#    print('func', level_index, point_index)

        if level_index == [1]*len(level_index):  #The very first point is a special case since it is just a constant
            string_parameter = 'P0=0.0d0'  # + str('{}'.format(surplus).replace('e', 'd'))
            parameters.append(string_parameter)
            string_hamil = 'P0' + '   ' + ' |  1'*len(level_index)
            hamiltonian.append(string_hamil)
        else:
            string_surpluses = ''
            for dim in range(len(level_index)):
                factor_scaling = (boundaries_coordinates[dim][1]-boundaries_coordinates[dim][0])
                shifting_scaling = boundaries_coordinates[dim][0] 
                factor_scaling_spacing = (boundaries_coordinates[dim][1]-boundaries_coordinates[dim][0]) 
                l = level_index[dim]
                j = point_index[dim]
                if l != 1:
                    string_surpluses += '' + str(dim+1)
                    string_surpluses += '' + str(l) + 'P' + str(j)
                spacing = 2.0**(-1.*l)
                spacing_scaled = spacing*factor_scaling_spacing
                position_point = j*spacing
                position_point_scaled = position_point*factor_scaling+shifting_scaling
                newlabel_pol = 'PD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=q['+str(position_point_scaled)+']'
                if j+1 == int(2**l):
                    newlabel_step = 'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=step['+str(shifting_scaling+(j-1)*spacing_scaled)+']' 
                elif j-1 == 0:
                    newlabel_step = 'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=rstep['+str(shifting_scaling+(j+1)*spacing_scaled)+']' 
                else: 
                    newlabel_step = 'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)+'=charfun['+str(shifting_scaling+(j-1)*spacing_scaled)+','+str(shifting_scaling+(j+1)*spacing_scaled)+']' 
                string_step = 'SD'+str(dim+1)+'L'+str(l)+'P'+str(j)
                if not(newlabel_pol in labels_pol):
                    labels_pol.append(newlabel_pol)
                if not(newlabel_step in labels_step):
                    labels_step.append(newlabel_step)
        #       To obtain the nd function, we call basisnd(np.array(*args), level_index , point_index)
        #       It is the product of each 1d function accross the dimensions
                if l != 1:
                    if level_type == 1: dim1 = dim
                    # We need the ancestors of this point
                    ancestors = [0.5]  #If want bounds --> add them here!!
                    for h in range(2, l):
                        if position_point > ancestors[-1]:
                            ancestors.append(ancestors[-1]+2.0**(-1.*h))
                        elif position_point < ancestors[-1]:
                            ancestors.append(ancestors[-1]-2.0**(-1.*h))
                    ancestors = np.array(ancestors)  # Contains the position of the ancestors accross the 1D projection
                    ancestors_scaled = np.array(ancestors)*factor_scaling+shifting_scaling
                    coeff = np.prod(1./(position_point_scaled-ancestors_scaled))
                    surplus = surplus*coeff
                    #Need to figure out what are the names of the ancestor polynomials
                    ancestor_level = 1
                    string_hamil += '|' + str(dim+1) + '   ' 
                    for point in ancestors:
                        string_hamil += 'PD' + str(dim+1) + 'L' + str(ancestor_level) + 'P' + str(int(point/(2.0**(-1.*ancestor_level)))) + '*'
                        ancestor_level += 1 
                    string_hamil += string_step + '   '
            string_hamil = str('{:.8e}'.format(surplus).replace('e', 'd')) + '   ' + string_hamil
            if level_type == 1: d_cuts[dim1].append(string_hamil) 
            hamiltonian.append(string_hamil)
             
    i += 1



op_1='''OP_DEFINE-SECTION
title
HONO 
end-title
end-op_define-section

PARAMETER-SECTION'''

print(op_1)

for j in parameters: 
    print(j)

op_2='''q20  = 2.696732586
q30  = 1.822912197
q10  = 2.213326419
q11  = 1.8653
th20 = 1.777642018
th10 = 1.9315017
mh   = 1.0, H-mass
mc   = 12.0,AMU
mo   = 15.9949,AMU
mn   = 13.9939,AMU
M11  = 1.0/mo+1.0/mh
M22  = 1.0/mo+1.0/mn
M33  = M22
M13  = 1.0/mo
M23  = -1.0/mn
p1   = PI/2.0
p2   = 3.0*PI/2.0
end-parameter-section
LABELS-SECTION'''

print(op_2)


for k in labels_pol:
    print(k)

for k in labels_step:
    print(k)

op_3='''g0pi = gauss[2.0,0.0]
g1pi = gauss[2.0,PI]
g2pi = gauss[2.0,2.0*PI]
qs1  = qs[1.0]
end-labels-section
HAMILTONIAN-SECTION
-----------------------------------'''

print(op_3)
label_modes = 'modes   '
for j in range(len(level_index)):
    label_modes += '| ' + 'x_' + str(j)
print(label_modes)

print('''-----------------------------------
-M11/2.0  | 1      |   1    |  dq^2   |   1         |    1         | 1
-M22/2.0  | dq^2   |   1    |   1     |   1         |    1         | 1
-M33/2.0  | 1      |   dq^2 |   1     |   1         |    1         | 1
-M13      | 1      |   dq   |   dq    |   1         |    q         | 1
 M23      | dq     |   dq   |   1     |   q         |    1         | 1
-M13      | 1      |   q^-1 |   q^-1  |   1         |    q         | 1
 M23      | q^-1   |   q^-1 |   1     |   q         |    1         | 1

-M13      |  1      |  q^-1 |   dq    |   1         |  udq2        | 1
 M13      |  1      |  q^-1 |   dq    |  udq        |    qs1       | cos
-M23      |  dq     |  q^-1 |   1     |  qs1        |  udq         | cos
 M23      |  dq     |  q^-1 |   1     |  udq2       |    1         | 1
-M13      |  1      |  dq   |   q^-1  |   1         |  udq2        | 1
 M23      |  q^-1   |  dq   |   1     |  udq2       |    1         | 1

 M13      | 1       | q^-1  |   dq    |  q*qs1^-1   |   qs1        | sdq
-M23      | dq      | q^-1  |   1     |  qs1        |   q*qs1^-1   | sdq

-M11/2.0  | 1       |  1    |   q^-2  |     1       | dq*qs1^2*dq  | 1
-M33/2.0  | 1       |  q^-2 |    1    |     1       | dq*qs1^2*dq  | 1
 M13      | 1       |  q^-1 |   q^-1  |     1      | dq*qs1^2*q*dq | 1
-M22/2.0  | q^-2    |  1    |   1     | dq*qs1^2*dq |     1        | 1
-M33/2.0  | 1       |  q^-2 |   1     | dq*qs1^2*dq |     1        | 1
-M23      | q^-1    |  q^-1 |   1     | dq*qs1^2*q*dq |   1        | 1

-M13/2.0  | 1       |  q^-1 |   q^-1  | qs1*dq      | dq*q*qs1     | cos
-M13/2.0  | 1       |  q^-1 |   q^-1  | dq*qs1      | q*qs1*dq     | cos
 M23/2.0  | q^-1    |  q^-1 |    1    | q*qs1*dq    | dq*qs1       | cos
 M23/2.0  | q^-1    |  q^-1 |    1    | dq*q*qs1    | qs1*dq       | cos
 M33/2.0  |  1      |  q^-2 |    1    | qs1*dq      | dq*qs1       | cos
 M33/2.0  |  1      |  q^-2 |    1    | dq*qs1      | qs1*dq       | cos

-M13/2.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | dq*q*qs1     | sdq
-M13/2.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | q*qs1*dq     | sdq
-M13/4.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | q^2*qs1^-1   | cos
 M13/4.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | qs1          | cos
 M23      |  q^-1   |  q^-1 |    1    | qs1^-1      | udq          | sdq
-0.25*M23 |  q^-1   |  q^-1 |    1    |  qs1^-1     |   q*qs1^-1   | cos

 M33      |  1      |  q^-2 |    1    | q*qs1^-1    | udq          | sdq

 M23/2.0  |  q^-1   |  q^-1 |    1    | dq*q*qs1    | q*qs1^-1     | sdq
 M23/2.0  |  q^-1   |  q^-1 |    1    | q*qs1*dq    | q*qs1^-1     | sdq
 M23/4.0  |  q^-1   |  q^-1 |    1    | q^2*qs1^-1  | q*qs1^-1     | cos
-M23/4.0  |  q^-1   |  q^-1 |    1    | qs1         | q*qs1^-1     | cos
-M13      |  1      |  q^-1 |   q^-1  | udq         | qs1^-1       | sdq
-M13/4.0  |  1      |  q^-1 |   q^-1  | q*qs1^-1    | qs1^-1       | cos

 M33      |  1      |  q^-2 |    1    | udq         | q*qs1^-1     | sdq

-M11/2.0  |  1      |  1    |   q^-2  |    1        |   qs1^-2     | dq^2
 M13/2.0  |  1      |  q^-1 |   q^-1  |  q*qs1^-1   |   qs1^-1     | cos*dq^2
 M13/2.0  |  1      |  q^-1 |   q^-1  |  q*qs1^-1   |   qs1^-1     | dq^2*cos
 M13/2.0  |  1      |  q^-1 |   q^-1  |  q*qs1^-1   |   qs1^-1     | cos
 M13      |  1      |  q^-1 |   q^-1  |    1        |   q*qs1^-2   | dq^2

-M22/2.0  |  q^-2   |  1    |    1    |  qs1^-2     |     1        | dq^2
-M23/2.0  |  q^-1   |  q^-1 |    1    |  qs1^-1     |   q*qs1^-1   | dq^2*cos
-M23/2.0  |  q^-1   |  q^-1 |    1    |  qs1^-1     |   q*qs1^-1   | cos*dq^2
-M23      |  q^-1   |  q^-1 |    1    |  q*qs1^-2   |     1        | dq^2
-M33/2.0  |  1      |  q^-2 |    1    |    1        | q^2*qs1^-2   | dq^2
-M33/2.0  |  1      |  q^-2 |    1    |  q^2*qs1^-2 |     1        | dq^2
-M33/2.0  |  1      |  q^-2 |    1    |  q*qs1^-1   |   q*qs1^-1   | dq^2*cos
-M33/2.0  |  1      |  q^-2 |    1    |  q*qs1^-1   |   q*qs1^-1   | cos*dq^2
''')

for k in hamiltonian:
    print(k)
op_4='''-----------------------------------
end-hamiltonian-section
'''
print(op_4)

cut_string=[''' 5.862318d-6   | q^-1    |  1      | 1       | 1        | 1          | 1
-3.674923d-5   | dq^2    |  1      | 1       | 1        | 1          | 1''',
'''
 1.152891d-5   | 1       | q^-1    | 1       | 1        | 1          | 1
-3.6749237d-5  | 1       | dq^2    | 1       | 1        | 1          | 1
''',
'''
 3.243973d-6   | 1       |  1      | q^-1    | 1        | 1          | 1
-2.893093d-4   | 1       |  1      | dq^2    | 1        | 1          | 1
''',
'''
-6.56776d-6      |  1      |  1      |  1   | q             |   1      | 1
-1.25549d-5      |  1      |  1      |  1   | dq*qs1^2*dq   |   1      | 1
 6.56776d-6      |  1      |  1      |  1   | dq*q*qs1^2*dq |   1      | 1
''',
'''
-7.07777d-6    | 1       |  1      | 1       | 1      |  q            | 1
-9.06079d-5    | 1       |  1      | 1       | 1      | dq*qs1^2*dq   | 1
 7.07777d-6    | 1       |  1      | 1       | 1      | dq*q*qs1^2*dq | 1
''',
'''
-1.14605d-4    | 1       |  1      | 1       | 1        | 1       | dq^2
-3.07824d-6    | 1       |  1      | 1       | 1        | 1       | dq^2*cos
-3.07824d-6    | 1       |  1      | 1       | 1        | 1       | cos*dq^2
0.005 |6 g0pi
0.005 |6 g2pi
''',
'''
-1.14605d-4    | 1       |  1      | 1       | 1        | 1       | dq^2
-3.07824d-6    | 1       |  1      | 1       | 1        | 1       | dq^2*cos
-3.07824d-6    | 1       |  1      | 1       | 1        | 1       | cos*dq^2
0.006 |6 g1pi
''']


for j in range(len(level_index)-1):
    print('hamiltonian-section_cut.' + str(j+1))
    print('-----------------------------------')
    print(label_modes)
    print('-----------------------------------')
    print(cut_string[j])
    for k in d_cuts[j]:
        print(k)
    print('end-hamiltonian-section')
print('hamiltonian-section_cut.6trans')
print('-----------------------------------')
print(label_modes)
print('-----------------------------------')
print(cut_string[5])
for k in d_cuts[-1]:
    print(k)
print('end-hamiltonian-section')
print('hamiltonian-section_cut.6cis')
print('-----------------------------------')
print(label_modes)
print('-----------------------------------')
print(cut_string[6])
for k in d_cuts[-1]:
    print(k)
print('end-hamiltonian-section')




print('end-operator')
#print(hamiltonian)

print('This dataset contains ', i, ' points')

