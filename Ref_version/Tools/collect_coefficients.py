import itertools
import sys
import yaml
import ast
import numpy as np
sys.path.append('Source/')

from basisnd import unique_points_term,position_processing,evaluate_model,basis_term,level_type_processing

fname_model   = sys.argv[1]
fname_dict    = sys.argv[2]
fname_restart = sys.argv[3]

restart_dict = yaml.load(open(fname_restart, 'r'), Loader=yaml.Loader)
surplus_dict = yaml.load(open(fname_dict, 'r'), Loader=yaml.Loader)
model = yaml.load(open(fname_model, 'r'), Loader=yaml.Loader)


def collect_model(model, surplus_dict, restart_dict):
    primitive_model = {}
    terms = restart_dict['coordinates_variations']
    minimum_coordinates = restart_dict['minimum_coordinates']
    dimension = len(terms[0])
    unique_points_1d = []
    term = [0]*dimension
    for i in range(dimension):
        term[i] = 1
        unique_points_1d_i, trash = unique_points_term(term, surplus_dict, restart_dict)
        term[i] = 0
        unique_points_1d.append(unique_points_1d_i[0])
    unique_points_1d_r = []
    for i in range(dimension):
        r = [ round(elem, 12) for elem in unique_points_1d[i] ]
        unique_points_1d_r.append(r)

    internal_limits = restart_dict['internal_limits']
    b_type = restart_dict['b_type']
    for term in terms:
        print('**** term ****', term)
        unique_points, unique_points_index = unique_points_term(term, surplus_dict, restart_dict)
        point_primitive = []
        coeff_basis = basis_term(term, unique_points, restart_dict)
        #We get all combinations of the indexes
        indexes = []
        unique_points_r = []
        for j in range(sum(term)):
            indexes.append(range(len(unique_points[j])))
            r = [ round(elem, 12) for elem in unique_points[j]]
            unique_points_r.append(r)
        t = 0
        for j in range(dimension):
            if term[j] == 1:
                p = [ unique_points_1d_r[j].index(round(point,12)) for point in unique_points_r[t] ]
                point_primitive.append(p)
                t += 1
        #The combination of indexes are associated with specific points: 
        all_combinations = list(itertools.product(*indexes))
        k = 0
        for combination_primitive in all_combinations:
#            sumk = [0.0]*sum(term)
            sumk = 0.0 
            coeff = 0.0
            combination_point_1d_primitive = []
            t = 0
            for dim in range(dimension):
                if term[dim] == 1:
                    combination_point_1d_primitive.append(point_primitive[t][combination_primitive[t]])
                    t += 1
            for combination_lagrange in all_combinations:
                #We need to figure out the level_index and point_index of this point ...
                point_index = []
                level_index = []
                i = 0
                for dim in range(dimension):
                    if term[dim] == 1:
                        level_index.append(unique_points_index[i][combination_lagrange[i]][0])
                        point_index.append(unique_points_index[i][combination_lagrange[i]][1])
                        i += 1
                    else:
                        level_index.append(1)
                        point_index.append(1)
                if level_type_processing(level_index) == sum(term): 
                    position_point_nd = []
                    for index in range(dimension):
                        spacing=2.0**(-1.*level_index[index])
                        position_point = point_index[index]*spacing
                        position_point_nd.append(position_point)
                    position_point_nd = position_processing(position_point_nd, internal_limits) #In the "real" space
                    key = str(list(level_index))+str(list(point_index))
#                    print('looking for point', key)
                    if key in surplus_dict: #Check that the point exist
                        coeff_lagrange = model[key]
                    else:
                        coeff_lagrange = 0.0
#                print(combination_lagrange, coeff_lagrange)
                    t = 0
                    prodk = 1.0
                    for i in range(dimension):
                        if term[i] == 1:
                            point_order = unique_points_r[t].index(round(position_point_nd[i],12))
                            prodk = prodk * coeff_basis[t][combination_primitive[t],point_order]
#                            sumk[t] += coeff_basis[t][combination_primitive[t],point_order]*abs(coeff_lagrange)**(1/sum(term))*sign(coeff_lagrange) 
                            t += 1
                    sumk += prodk * coeff_lagrange
            if sum(term) == 0:  #The first constant
                key = str(list([1]*dimension))+str(list([1]*dimension))
                coeff = model[key]
            else:
                coeff = np.prod(sumk)
#            if sum(term)==1: 
            if coeff != 0.0:
                key = str(term)+str(list(combination_primitive))+str(list(combination_point_1d_primitive))
                primitive_model.update({key: float(coeff)})
                print(term, list(combination_primitive), str(combination_point_1d_primitive), coeff)
    return primitive_model
def sign(num):
    return -1 if num < 0 else 1

primitive_model = collect_model(model, surplus_dict, restart_dict)

fname_model = 'primitive_model'

file_model = open(fname_model, "w") 
yaml.dump(primitive_model, file_model)
file_model.close()
