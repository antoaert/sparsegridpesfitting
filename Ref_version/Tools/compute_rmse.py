import sys
sys.path.append('Source/')
import yaml
from basisnd import *


filename_expansion = sys.argv[1]
restartname_expansion = sys.argv[2]

expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
restart = yaml.load(open(restartname_expansion, 'r'), Loader=yaml.Loader)

active_index_set = restart['ActiveIndexSet']
energy_threshold = restart['energy_threshold']


residual_error = error_of_a_set_rmse(active_index_set, expansion, energy_threshold, Verbose=True)
print('------------------------------------------------------------------')
residual_error_nd = error_of_a_set_nd(active_index_set, expansion, energy_threshold, Verbose=True)
print('------------------------------------------------------------------')


print(residual_error, 'au', residual_error*219474., 'cm-1')
print(residual_error_nd, 'error times support of the interpolation primitives')
