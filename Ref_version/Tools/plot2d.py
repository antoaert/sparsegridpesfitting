import sys
sys.path.append('Source/')
import yaml
from scipy.optimize import fmin
from functools import partial
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np


from basisnd import potential_nd
 
filename_expansion = sys.argv[1]
restartname_expansion = sys.argv[2]

expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
restart = yaml.load(open(restartname_expansion, 'r'), Loader=yaml.Loader)

boundaries_coordinates = restart['boundaries']
internal_limits = restart['internal_limits']

guess_minimum = [2.53412273, 2.26578162, 2.06547065, 1.82736516, -0.56928029, -0.34693643, -0.28903256, -3.14159400, 0.00000000]

minimum = fmin(partial(potential_nd, surplus_dict=expansion, internal_limits=internal_limits, boundaries_coordinates=boundaries_coordinates), guess_minimum)

print(list(minimum))


#GENERATE A 1D TERM
batchsize=3000 #number of points to plot
COORDINATES=[0,8]

xy_min = [boundaries_coordinates[COORDINATES[0]][0],boundaries_coordinates[COORDINATES[1]][0]]
xy_max = [boundaries_coordinates[COORDINATES[0]][1],boundaries_coordinates[COORDINATES[1]][1]]

points_on_grid=np.random.uniform(low=xy_min, high=xy_max, size=(batchsize,2))


y_out=np.zeros(batchsize)
t=0
for point2d in points_on_grid: 
  point=np.zeros(len(minimum))
  for j in range(len(minimum)):
    if j == COORDINATES[0]:
      point[j] = point2d[0]
    elif j == COORDINATES[1]:
      point[j] = point2d[1]
    else:
      point[j] = minimum[j] 
  y_out[t]=potential_nd(point, expansion, internal_limits, boundaries_coordinates)
  t+=1

x=points_on_grid[:,0]
y=points_on_grid[:,1]
z=y_out

fig = plt.figure()
ax = Axes3D(fig)
surf = ax.plot_trisurf(x, y, z, cmap=cm.jet, linewidth=0.1)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()


