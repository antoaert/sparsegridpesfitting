import numpy as np
from itertools import permutations
from active_indexes import *
def bounded_permutations(lst, bounds, perm=[], index=0):
    if index == len(lst):
        yield tuple(perm)
    else:
        for i in range(1, min(lst[index], bounds[index]) + 1, 2):  # start from 1 and step by 2 to get odd numbers
            yield from bounded_permutations(lst, bounds, perm + [i], index + 1)

# usage:
#list(bounded_permutations([3, 2, 1], [2, 2, 2]))


def points_to_compute(l_index, dimension, internal_limits, boundaries_coordinates, spatial_adaptivity, error_threshold, surpluses_dic, SamplingMode):
    #We need to figure out what are the points that belong to such level index and that may possess the necessary criteria to be computed
    level_index=np.array(l_index).astype(int)
    positions = []
    points_index = []
#    perm_list = []
    
#    for j in range(dimension): 
#        for i in range(1,2**level_index[j]):
#            if i%2 != 0:
#                perm_list.append(i)
    power_level_index = [2**level for level in level_index]
    permutations_list = list(bounded_permutations(power_level_index, power_level_index))
    level_type = 0
    points_in_dictionnary = 0
    for index in level_index:
        if index > 1:
            level_type += 1
    for point_index in unique_lists_fast(permutations_list):
    #There can be multiple reasons why one point is not added to the dictionnary,
    #The permutations include points that do not exist, they are beyond the given grid
        position_point_nd = []
        position_point_scaled = []
        if all(point_index[j] < 2**level_index[j] for j in range(dimension)):  #Points have to belong to the grid
            for d in range(dimension):
                spacing = 2.0**(-1.*level_index[d])
                position_point = point_index[d]*spacing
                position_point_nd.append(position_point)
                position_point_scaled.append(position_point*(internal_limits[d][1]-internal_limits[d][0])+internal_limits[d][0])
            if all((position_point_scaled[h] >= boundaries_coordinates[h][0] and position_point_scaled[h] <= boundaries_coordinates[h][1] for h in range(dimension))) or (level_type < 3 and all(level_index[t] < 4 for t in range(dimension))): #Allows to compute some points outside of boundaries
                #Spatial adaptivity does not apply to the very first point
                key = str(list(level_index))+str(list(point_index))
                if (list([1]*dimension) == list(level_index)): 
                    if not(key in surpluses_dic): #This is useful if we restart
                        positions.append(position_point_nd)
                        points_index.append(point_index)
    
                elif not(key in surpluses_dic):  #Need to check that the point doesnt already exists in the dictionnary (This is useful if we restart the computation) 
#                else: #If spatial adaptivity is a criterion, then we need to figure out what are the ancestor points in the direct parent grids
                       #Even if we do not use the spatial adaptivity, we need to figure out what are the ancestor points because the boundaries are not perfectly symmetric. 
                       #Therefore, we might end up with points at some point indexes in the grid that do not have their ancestors (because they are beyond the boundaries!)
                    #No need to obtain ancestors for the very first level index (all ones) therefore is included in preceding if statement
                    ancestors_lists=[]
                    for dim in range(dimension):
                        spacing=2.0**(-1.*level_index[dim])
                        position_point=point_index[dim]*spacing
                        #Figure out what are the parent points
                        ancestors_pos=[0.5] #If want bounds --> add them here!!
                        for h in range(2,level_index[dim]):
                           if position_point > ancestors_pos[-1]:
                               ancestors_pos.append(ancestors_pos[-1]+2.0**(-1.*h))
                           elif position_point < ancestors_pos[-1]:
                               ancestors_pos.append(ancestors_pos[-1]-2.0**(-1.*h))
                        ancestors_pos=np.array(ancestors_pos)
                
                        ancestors=list()
                        ancestors.append([1,1])
                        for i in range(2, level_index[dim]) : 
                            ancestors.append([i, int(ancestors_pos[i-1]/(2.0**(-1.*i)))]) 
                        #Need to include itself for each dimension
                        ancestors.append([level_index[dim], point_index[dim]])
                        ancestors_lists.append(ancestors) 
                    ancestors_indexes = []
                    for el in parent_indexes(level_index, dimension):
                        ancestors_indexes.append(list(np.array(el).astype(int)))
                    ancestors_points_indexes=[]
                    for ancestor_index in ancestors_indexes: 
                        ancestors_point_index = []
                        for dim in range(dimension):
                            ancestors_point_index.append(int(ancestors_lists[dim][int(ancestor_index[dim]-1)][1]))
                        ancestors_points_indexes.append(list(np.array(ancestors_point_index).astype(int)))
#   #                 print('direct ancestor points are', ancestors_points_indexes)
                    #We need to check if all the direct ancestors exist in the dictionnary
                    if SamplingMode or all(str(list(ancestors_indexes[j]))+str(list(ancestors_points_indexes[j])) in surpluses_dic for j in range(len(ancestors_indexes))):
                        #All points that belong to the grid are added if the spatial adaptivity is not a criterion as long as the ancestors exist
                        if not(spatial_adaptivity):
                            positions.append(list(position_point_nd))
                            points_index.append(list(point_index))
                        else:
                        #We need to check wether their estimated error is above the threshold if we use the spatial adaptivity 
                            if all(abs(surpluses_dic[str(list(ancestors_indexes[j]))+str(list(ancestors_points_indexes[j]))][1]) > error_threshold for j in range(len(ancestors_indexes))):
                                positions.append(list(position_point_nd))
                                points_index.append(list(point_index))

                else:
                    points_in_dictionnary += 1 
    #We scale the position of the point to compute the function in the span of the real coordinates
    for i in range(len(positions)):
        for j in range(dimension):
#            positions[i][j] = positions[i][j]*(boundaries_coordinates[j][1]-boundaries_coordinates[j][0])+boundaries_coordinates[j][0]
            positions[i][j] = positions[i][j]*(internal_limits[j][1]-internal_limits[j][0])+internal_limits[j][0]

        

    return positions, points_index, points_in_dictionnary
