import numpy as np


def smolyak_1dgrid(l,**kwargs):
    """
    Generate a Smolyak 1d sparse grid.

    Parameters:
    - l: the level of the grid that has to be provided 
    - (optional) GRID: the type of grid to be used. (Default=regular)
    - (optional) BOUNDS: treatment of boundaries or not. (Default=False)

    Returns:
    - grid_points: A NumPy array containing the grid points positions.
    """
    grid_type=kwargs.get('GRID', 'regular')
    bounds_add=kwargs.get('BOUNDS', False)

    # Check if l is valid
    if l < 1:
        raise ValueError("The level can only be 1 or greater")

    # Create initial grid
    grid_points=[]

    if l == 1 and bounds_add:
        grid_points.append(0.)
        grid_points.append(1.)

    if grid_type=='regular':
        for i in range(2**l):
            if i%2!=0: 
                grid_points.append(i*2.0**(-1.*l))

    grid_points=np.array(grid_points) 

    return grid_points

