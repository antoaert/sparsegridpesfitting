import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import ast
import sys
import os

sys.path.append(os.path.abspath('..'))

from scipy.optimize import minimize, least_squares

from functools import partial

from itertools import permutations, combinations
import itertools

from basis1dsrc import *
from basisnd import *
from active_indexes import *
from my_function_to_learn import *

def minimize_Vandermondian(b_type, x_ref, alpha, positions_of_cut):
#    best_alpha = minimize(partial(basis1d_carr_spatial_det, b_type = b_type, x_ref = x_ref, positions_of_cut = positions_of_cut), alpha)#, bounds=tuple(0.1,7.0))
    best_alpha = least_squares(partial(basis1d_carr_spatial_det, b_type = b_type, x_ref = x_ref, positions_of_cut = positions_of_cut), alpha)#, bounds=(0.5, 4.0))
    return best_alpha['x']

def smoothing_objective(b_type, x_ref, alpha, positions_of_cut, bounds, term, restart_dict, dim):
    best_alpha = least_squares(partial(smoothness, bound_up=bounds[1], bound_down=bounds[0], positions_of_cut=positions_of_cut, x_ref=x_ref, b_type=b_type, term=term, restart_dict=restart_dict, dim=dim), alpha[dim], bounds=(0.1, 25.))
    return best_alpha['x']

def smoothness(alpha, bound_up, bound_down, positions_of_cut, x_ref, b_type, term, restart_dict, dim):
    alphaa = restart_dict['alpha']
    alphaa[dim] = alpha
    restart_dict.update({'alpha': alphaa})
    coefficients = basis_term(term, positions_of_cut, restart_dict)  
    primitive = lambda x: sum(basis1d_eval(b_type, x_ref, coefficients[0][i], alpha)(x) for i in range(len(coefficients[0])))
    npoints = 300
    x = np.zeros(npoints)
    y = np.zeros(npoints)
    for i in range(npoints):
        xpoint = bound_down + (bound_up-bound_down)/npoints*float(i)
        x[i] = xpoint
        y[i] = primitive(xpoint)
    dy=np.diff(y,1)
    dx=np.diff(x,1)
    yfirst=dy/dx
    xfirst=0.5*(x[:-1]+x[1:])
    dyfirst=np.diff(yfirst,1)
    dxfirst=np.diff(xfirst,1)
    ysecond=dyfirst/dxfirst
    xsecond=0.5*(xfirst[:-1]+xfirst[1:])
    smoothness = 0.0
    for j in range(npoints-3):
        smoothness += ysecond[j]**2.*(xsecond[j+1]-xsecond[j]) 
    return smoothness


def min_domain(level_index, position_x, surplus_dict, internal_limits, boundaries_coordinates):
    domain = []
    for i in range(len(position_x)):
        factor_scaling = (internal_limits[i][1]-internal_limits[i][0])
        if level_index[i] == 1:
            domain.append((position_x[i]-0.001, position_x[i]+0.001))
        else: 
            domain.append((position_x[i]-1.*factor_scaling/2.**level_index[i], position_x[i]+1.*factor_scaling/2.**level_index[i]))
#    print(position_x)
#    print(domain)
    minimum = minimize(partial(potential_nd, surplus_dict=surplus_dict, \
    internal_limits=internal_limits, boundaries_coordinates=boundaries_coordinates), \
    position_x, bounds = tuple(domain))
    return minimum['fun'] 

def min_domain_base_model(level_index, position_x, surplus_dict, internal_limits, boundaries_coordinates):
    domain = []
    for i in range(len(position_x)):
        factor_scaling = (internal_limits[i][1]-internal_limits[i][0])
        if level_index[i] == 1:
            domain.append((position_x[i]-0.001, position_x[i]+0.001))
        else: 
            domain.append((position_x[i]-1.*factor_scaling/4.**level_index[i], position_x[i]+1.*factor_scaling/4.**level_index[i]))
    minimum = minimize(base_model, \
    position_x, bounds = tuple(domain))
    return minimum['fun'] 

def min_domain_func(position_x, id1, level_index, surplus_dict, internal_limits, boundaries_coordinates,error_threshold , potential_function):
    domain = []
    #The functional form of the potential needs scaled positions
    for i in range(len(position_x)):
        factor_scaling = 1. #(internal_limits[i][1]-internal_limits[i][0])
        if level_index[i] == 1:
            domain.append((position_x[i]-0.001, position_x[i]+0.001))
        else: 
            domain.append((position_x[i]-1.*factor_scaling/2.**level_index[i], position_x[i]+1.*factor_scaling/2.**level_index[i]))
#            print('domain',(position_x[i]-1.*factor_scaling/2.**level_index[i], position_x[i]+1.*factor_scaling/2.**level_index[i]))
    minimum = minimize(potential_function, position_x, bounds = tuple(domain), tol = error_threshold, method = 'slsqp')
#    print(minimum['fun'])
    return minimum['fun'], id1 

def min_domain_func_lagrange(position_x, id1, level_index, surplus_dict, internal_limits, boundaries_coordinates,error_threshold , potential_function):
    domain = []
    points = []
    dimension = len(position_x)
    for i in range(dimension):
        factor_scaling = (internal_limits[i][1]-internal_limits[i][0])
        if level_index[i] == 1:
            domain.append([position_x[i]]) 
        else: 
            domain.append([position_x[i]-1.*factor_scaling/2.**level_index[i], position_x[i]+1.*factor_scaling/2.**level_index[i]])
    points = list(itertools.product(*domain)) #combinations(*domain)
#    print(points)
    energies = np.zeros(len(points))
    for j in range(len(points)):
        energies[j] = potential_function(points[j])
    minimum = np.min(energies) 
    #minimum = minimize(potential_function, position_x, bounds = tuple(domain), tol = error_threshold, method = 'slsqp')
#    print(minimum['fun'])
    return minimum, id1 
