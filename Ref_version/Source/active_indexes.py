import numpy as np
import matplotlib.pyplot as plt
import itertools
def unique_rows(a):
    a = np.ascontiguousarray(a)
    unique_a = np.unique(a.view([('', a.dtype)]*a.shape[1]))
    return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))

def unique_lists(list_of_lists):
# Your initial list of lists
#list_of_lists = [[0., 2.], [1., 1.], [2., 0.], [0., 2.], [1., 1.]]
    # Convert each inner list to a tuple
    list_of_tuples = [tuple(lst) for lst in list_of_lists]
    # Convert the list of tuples to a set to remove duplicates
    set_of_tuples = set(list_of_tuples)
    # Convert the set of tuples back to a list of lists
    unique_list_of_lists = [list(tpl) for tpl in set_of_tuples]
    return unique_list_of_lists

def unique_lists_fast(list_of_lists):
    # Convert each inner list to a tuple and then to a set to remove duplicates
    set_of_tuples = set(tuple(lst) for lst in list_of_lists)
    # Convert the set of tuples back to a list of lists
    unique_list_of_lists = [list(tpl) for tpl in set_of_tuples]
    return unique_list_of_lists


def next_indexes(current_active_index, dimension):
    new_indexes=[]
    if len(current_active_index) == 0 : #if empty, need to initialize the index set
        active_set=np.ones(dimension)
        new_indexes.append(list(active_set))
    else :
        for j in range(dimension) : #Find the neighbours
            active_set=np.zeros(dimension)
            active_set[j]=1
            new_indexes.append(list(active_set+current_active_index))
    return unique_lists(new_indexes)

def parent_indexes(current_active_index, dimension):
    parent_indexes=[]
    for j in range(dimension) : #Find the neighbours
        active_set=np.zeros(dimension)
        active_set[j]=1
        if current_active_index[j]>1:
            parent_indexes.append(list(current_active_index-active_set))
    return unique_lists(parent_indexes)

"""
dim=3
#Verbose = False
Verbose = True
#Initialize the indexes
active_index_set=[]
old_index_set=[]

#All next indexes are admissible at this stage and are active 
admissible_indexes=next_indexes(active_index_set, dim)

active_index_set.append(*admissible_indexes)
#Compute initial residual error
#TODODODODODODODO
# It is function: error_measure_1d(level, surplus_dict) 

#Compute points on the given grids!
#TODODODODODODODO



#While residual error is above threshold:

#Within the active level indexes find the one with largest residual error:
current_index=[1, 1, 1]
if Verbose: print('active index set:', active_index_set)
old_index_set.append(current_index)
active_index_set.remove(current_index)

new_indexes=next_indexes(current_index, dim) #The last argument if the number of dimensions

#Figure out which of these indexes are admissible and add them to the active set:
admissible_indexes=[]
for index in new_indexes :
    if Verbose : print('parents of', index, 'are/is', parent_indexes(index,dim)) 
    for parent in parent_indexes(index, dim) :
        if parent in old_index_set :
            continue
        else :
            break
    admissible_indexes.append(index) 
    active_index_set.append(index)
if Verbose : print('New active indexes:', admissible_indexes)

# Compute points on the given grids
# TODODODODODO
 



 
"""
