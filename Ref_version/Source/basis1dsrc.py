import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import mpmath



def position_processing(position_x, internal_limits):
    position = np.zeros(len(position_x))
    for dim in range(len(position_x)):
        factor_scaling = (internal_limits[dim][1]-internal_limits[dim][0])
        shifting_scaling = internal_limits[dim][0]
        position[dim] = (position_x[dim]*factor_scaling)+shifting_scaling
    return position

def basis1d_carr_spatial(alpha, b_type, x_ref, positions_of_cut):
    """ b_type: basis type , e.g. 'Morse', 'polynomial' ...
        x_ref: position of reference (chosen to be the reference point of the interpolation)
        i: index of the point of interest (index of the basis function)
        positions_of_cut: all the points within this 1D cut that construct the interpolation basis
    """
    if b_type == 'polynomial':
        primitive_function = lambda x, x_ref, k : (x-x_ref)**(k+1)
    if b_type == 'morse':
        primitive_function = lambda x, x_ref, k : (1.-np.exp(-1.0*alpha*(x-x_ref)))**(k+1)
    if b_type == 'trigonometric':
#        primitive_function = lambda x, x_ref, k : (np.sin(int(k/2)*(x-x_ref))) if k % 2 != 0 else (2.*(np.sin(0.5*(int(k/2)*(x-x_ref))))**2.) #int() behaves like math.floor()
        primitive_function = lambda x, x_ref, k : (np.sin(int((k+2)/2)*(x-x_ref))) if k % 2 != 0 else (2.*(np.sin(0.5*(int((k+2)/2)*(x-x_ref))))**2.) #int() behaves like math.floor()
    npoints = len(positions_of_cut)
    MatrixB=np.zeros((npoints, npoints))
    for i in range(npoints):
        for j in range(npoints):
            MatrixB[i,j]=primitive_function(positions_of_cut[i],x_ref,j) 
#    print(x_ref)
#    print(b_type)
#    print(MatrixB)


    MatrixC=np.linalg.inv(MatrixB)
#    MatrixC = mpmath.matrix(MatrixB)**-1

#    det = 1.
#    if determinant:
#        det = abs(np.linalg.det(MatrixB)) - 1.
#    lagrange_func = lambda x, i: sum(MatrixC[j,i-1]*primitive_function(x, x_ref, j) for j in range(npoints)) 
#    return lagrange_func
    return MatrixC

def basis1d_carr_spatial_det(alpha, b_type, x_ref, positions_of_cut):
    """ b_type: basis type , e.g. 'Morse', 'polynomial' ...
        x_ref: position of reference (chosen to be the reference point of the interpolation)
        i: index of the point of interest (index of the basis function)
        positions_of_cut: all the points within this 1D cut that construct the interpolation basis
    """
    if b_type == 'polynomial':
        primitive_function = lambda x, x_ref, k : (x-x_ref)**(k+1)
    if b_type == 'morse':
        primitive_function = lambda x, x_ref, k : (1.-np.exp(-1.0*alpha*(x-x_ref)))**(k+1)
    if b_type == 'trigonometric':
        primitive_function = lambda x, x_ref, k : (np.sin(int(k/2)*(x-x_ref))) if k % 2 != 0 else (2.*(np.sin(0.5*(int(k/2)*(x-x_ref))))**2.) #int() behaves like math.floor()
    npoints = len(positions_of_cut)
    MatrixB=np.zeros((npoints, npoints))
    for i in range(npoints):
        for j in range(npoints):
            MatrixB[i,j]=primitive_function(positions_of_cut[i],x_ref,j) 
#    MatrixC=np.linalg.inv(MatrixB)
    
#    det = abs(abs(mpmath.det(mpmath.matrix(MatrixB)))-1.)
    A = mpmath.matrix(MatrixB)
    det = float(mpmath.det(A))
    return abs(abs(det)-1.)

def basis1d_carr(b_type, x_ref, l, i_kept, j_kept, internal_limits):
    """ b_type: basis type , e.g. 'Morse', 'polynomial' ...
        x_ref: position of reference (chosen to be the reference point of the interpolation)
        l: level of the grid
        i_kept: grid level index of the points that are used
        j_kept: point level index of the 1d grid that are actually used
        boundaries: coordinates boundaries of this dimension
        internal limits are the boundaries that are effectively used by the interpolation procedure (including grid generation)
    """
    #The 1d basis is built for each point, however, the existence of all the other points
    #conditions the basis for each point ... it only has to be performed once for 
    #all points of the given level index


    if b_type == 'polynomial':
        primitive_function = lambda x, x_ref, k : (x-x_ref)**(k+1)
    if b_type == 'morse':
        primitive_function = lambda x, x_ref, k : (1.-np.exp(-0.7*(x-x_ref)))**(k+1)
    if b_type == 'trigonometric':
        primitive_function = lambda x, x_ref, k : (np.sin(int((k+2)/2)*(x-x_ref))) if k % 2 != 0 else (2.*(np.sin(0.5*(int((k+2)/2)*(x-x_ref))))**2.) #int() behaves like math.floor()
#        primitive_function = lambda x, x_ref, k : (np.sin(int(k/2)*(x-x_ref))) if k % 2 != 0 else (2.*(np.sin(0.5*(int(k/2)*(x-x_ref))))**2.) #int() behaves like math.floor()

    npoints = len(j_kept)
    points = np.zeros(npoints)
    #Generate the position of all the points of the current level and its ancestors (all of them):
    for k in range(npoints):
        spacing = 2.**(-1.*i_kept[k])
        points[k]=position_processing([j_kept[k]*spacing], [internal_limits])
    #This procedure to obtain lagrange type functions requires the position of the points in the "real" space.
    MatrixB=np.zeros((npoints, npoints))
    for i in range(npoints):
        for j in range(npoints):
            MatrixB[i,j]=primitive_function(points[i],x_ref,j) 
    MatrixC=np.linalg.inv(MatrixB)
    lagrange_func = lambda x, i: sum(MatrixC[j,i-1]*primitive_function(x, x_ref, j) for j in range(npoints)) 
    return lagrange_func

def basis1d(l,j,**kwargs):
    """
    Generate a  1d basis based on a regular grid.

    Parameters:
    - l: the level of the grid
    - j: the jth point on that grid where we want the basis function to be centered 

    Returns:
    - 
    """
    grid_type=kwargs.get('GRID', 'regular')

    # Check if l is valid
    if l < 1:
        raise ValueError("The level can only be 1 or greater")
    #Check if we ask for a basis located at a child point
    if j%2 == 0:
        raise ValueError("The point you are specifying is an ancestor and not a child or is beyond the limit of the grid") 

    if grid_type=='regular':
        if j>2**l :
            raise ValueError("This point is beyond the limit of the grid level")
        spacing=2.0**(-1.*l)
        position_point=j*spacing
#        print('point', position_point)
        #obtain the position of ancestors starting from the level 1 grid
        ancestors=[0.5] #If want bounds --> add them here!!
        for h in range(2,l):
           if position_point > ancestors[-1]:
               ancestors.append(ancestors[-1]+2.0**(-1.*h))
           elif position_point < ancestors[-1]:
               ancestors.append(ancestors[-1]-2.0**(-1.*h))
        ancestors=np.array(ancestors)
#        print('ancestors', ancestors)
#        print('spacing', spacing)
        if l==1 :
            func = lambda x : 1.
        else: 
            func = lambda x :  np.prod((x-ancestors)/(position_point-ancestors)) if abs(x-position_point) < spacing else 0.0 

    return func 


def integral_of_basis1d(func):
    return integrate.quad(func, 0., 1.)  #We integrate between bounds 0. and 1. (inefficient, but easier to implement)
def integral_of_basis1d_bd(func,bounds):
    return integrate.quad(func, bounds[0], bounds[1])  #We integrate between bounds 0. and 1. (inefficient, but easier to implement)


def get_surplus_1d(valf, l, j, surpluses, **kwargs) :
    """
    valf : function value
    l    : level 
    j    : point number
    surpluses : dictionnary that contains the points surpluses
    """
    Verbose=kwargs.get('Verbose', False)

    if j>2**l :
        raise ValueError("This point is beyond the limit of the grid level")
    spacing=2.0**(-1.*l)
    position_point=j*spacing
    #Figure out what are the parent points
    ancestors_pos=[0.5] #If want bounds --> add them here!!
    for h in range(2,l):
       if position_point > ancestors_pos[-1]:
           ancestors_pos.append(ancestors_pos[-1]+2.0**(-1.*h))
       elif position_point < ancestors_pos[-1]:
           ancestors_pos.append(ancestors_pos[-1]-2.0**(-1.*h))
    ancestors_pos=np.array(ancestors_pos)

    ancestors=list()
    ancestors.append([1,1])
    for i in range(2, l) : 
        ancestors.append([i, int(ancestors_pos[i-1]/(2.0**(-1.*i)))]) 
#    print('ancestors', ancestors)
    

    #The surplus is the difference between the value of the function and the value of the parent functions at that point. 
    new_surplus = valf
    for ancestor in ancestors :
        ancestor_func = basis1d(ancestor[0], ancestor[1]) #0 and 1 are the level and the point numbers
        if Verbose : print('ancestor_func_at_point', ancestor_func(position_point)) 
        dict_entry=surpluses[str(list((np.array([ancestor[0], ancestor[1]]))))]
        new_surplus=new_surplus-ancestor_func(position_point)*dict_entry[0]
    if Verbose : print('valf', valf, 'new_surplus', new_surplus) 
    
    error_estimated = np.abs(new_surplus*integral_of_basis1d(basis1d(l, j))[0])
    return new_surplus, error_estimated

def potential_1d(position_x, level_max, surplus_dict) :
    function_value = 0.0
    for level in range(1, level_max+1) : 
        for point in range(1, 2**level) :
            if point%2 !=0 :
                if str(list(np.array([level, point]))) in surplus_dict:        
                    surplus = surplus_dict[str(list(np.array([level, point])))][0]
                    function_basis = basis1d(level, point)
                    function_value += surplus*function_basis(position_x)
    return function_value

def error_measure_1d(level, surplus_dict):
    error_estimate=0.0
    for point in range(1, 2**level) :
        if point%2 !=0 :
            if str(list(np.array([level, point]))) in surplus_dict:
                error_estimate += surplus_dict[str(list(np.array([level, point])))][1]
    return error_estimate

"""
Verbose=False



if Verbose : print('integral of the basis', integral_of_basis1d(basis_func)[0])

#Values of the function are obtained from this one for now:
def target_func(x) :
    return(-75.0087+0.05*(1.-np.exp(-0.25*(x-0.54)))**2.)
#    return ((-1.99*np.cos(1-x*6.)*(x)))
#    return (x-0.5)**2.#+1.23



#Initialize dictionnary:
Surpluses={}
#Add the value at the very first point:
Surpluses.update({str(list(np.array([1,1]))) : [target_func(0.5), np.abs(target_func(0.5)*integral_of_basis1d(basis1d(1,1))[0])]}) #Surplus, error


l_max=6
error_threshold = 1e-05

for level in range(2, l_max+1) :
    for point in range(1, 2**level) :
        if point%2 !=0 : #The new points for that level are only the odd numbered points
            print('point indexes', level, point) 
            spacing=2.0**(-1.*level)    
            position_point=point*spacing
            #Compute the value of the function at the position: target_func(position_point)
            #Obtain the surplus and add it to the dictionnary:

            #If regional adaptivity is used, then this point must be computed only if the parent point error is above a threshold 
            #(1) Need to figure out which of the points is the latest ancestor:
            ancestors_pos=[0.5] #If want bounds --> add them here!!
            for h in range(2,level):
                if position_point > ancestors_pos[-1]:
                    ancestors_pos.append(ancestors_pos[-1]+2.0**(-1.*h))
                elif position_point < ancestors_pos[-1]:
                    ancestors_pos.append(ancestors_pos[-1]-2.0**(-1.*h))
            ancestors_pos = np.array(ancestors_pos)
            #From the position of the latest ancestor, obtain its index
            latest_ancestor = [level-1, int(ancestors_pos[-1]/(2.0**(-1.*(level-1))))]
            print('closest ancestor', latest_ancestor)
            #Compare the estimated error of the ancestor to the threshold, compute the point only if it is above.
            if Surpluses.get(str(list(latest_ancestor))) != None : 
                if Surpluses[str(list(latest_ancestor))][1] > error_threshold :
                    point_surplus, point_error = get_surplus_1d(target_func(position_point), level, point, Surpluses) 
                    Surpluses.update({str(list(np.array([level, point]))) : [point_surplus, point_error]})
                    print('point_position', position_point, 'point error', point_error)


#print(potential_1d(0.25, l_max, Surpluses))

print(Surpluses)

bounds_figure=(0.,1.)


#génération de la figure de la base donnée
x_train=np.random.uniform(low=bounds_figure[0],high=bounds_figure[1],size=5000) 
y_basis=np.zeros(len(x_train))
for xij in range(len(x_train)):
    y_basis[xij] = potential_1d(x_train[xij], l_max, Surpluses)


for  level in range(1, l_max+1) :
    print('level:', level, 'error:', error_measure_1d(level, Surpluses))


fig = plt.figure()
plt.plot(x_train,y_basis,marker='o',linestyle="")
plt.plot(x_train,target_func(x_train), marker='+', linestyle="")
plt.show()

'''
basis_func=basis1d(4,5)

bounds_figure=(0.,1.)


#génération de la figure de la base donnée
x_train=np.random.uniform(low=bounds_figure[0],high=bounds_figure[1],size=5000) 
y_basis=np.zeros(len(x_train))
for xij in range(len(x_train)):
    y_basis[xij] = basis_func(x_train[xij])



fig = plt.figure()
plt.plot(x_train,y_basis,marker='o',linestyle="")
plt.show()

'''
"""
