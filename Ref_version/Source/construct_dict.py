import numpy as np
import matplotlib.pyplot as plt
import os
import shutil
import time
import yaml


from itertools import permutations
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from concurrent.futures import as_completed


from basis1dsrc import *
from active_indexes import *
from basisnd import *
from points_to_be_computed import *
from minimum_procedures import * 
from my_function_to_learn import *


def main(dimension, n_workers, error_threshold, energy_threshold, spatial_adaptivity, filename_expansion, filename_restart_file, filename_model, boundaries_coordinates, minimum_coordinates, Verbose, max_order, DeltaFitting, IsThisARestart, LagrangeFlag, b_type, SamplingMode, sampled_points):

    os.system("touch ./stop")    
    total_number_of_points = 0 
    #OLD    error_type = 'nd'  #Two options, rmse or nd
    #Overall convergence is RMSE, selection criterion is nd
    exe=ThreadPoolExecutor(max_workers=n_workers)

    #We want the center of the grids to be associated with the reference geometry given by minimum_coordinates
    #For this reason, we define internal limits
    internal_limits = []
    alpha = [1.]*dimension
    for dim in range(dimension):
        print('---- DIMENSION ', dim+1, '----')
        r_spacing = abs(boundaries_coordinates[dim][1]-minimum_coordinates[dim])  #right spacing 
        l_spacing = abs(boundaries_coordinates[dim][0]-minimum_coordinates[dim])  #left spacing
        if r_spacing > l_spacing:
            internal_limits.append((minimum_coordinates[dim]-r_spacing, minimum_coordinates[dim]+r_spacing))
        else:
            internal_limits.append((minimum_coordinates[dim]-l_spacing, minimum_coordinates[dim]+l_spacing))
        print('Current boundaries', boundaries_coordinates[dim])
        print('Internal limits used are', internal_limits[dim])


    #We want to keep the computed points in a dictionnary, also the constructed interpolation
    if os.path.exists(filename_expansion):
        current_time = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime())
        shutil.copy(filename_expansion, filename_expansion + "-" + current_time + ".copy")
        print(f"The file '{filename_expansion}' already exists and a copy is kept at '{filename_expansion}.copy'")

    if os.path.exists(filename_restart_file):
        current_time = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime())
        shutil.copy(filename_restart_file, filename_restart_file + "-" + current_time + ".copy")
        print(f"The file '{filename_restart_file}' already exists and a copy is kept at '{filename_restart_file}.copy'")

    if os.path.exists(filename_model):
        current_time = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime())
        shutil.copy(filename_model, filename_model + "-" + current_time + ".copy")
        print(f"The file '{filename_model}' already exists and a copy is kept at '{filename_model}.copy'")

    #Initialization of internal quantities 
    id1 = 0
    init = True

    if IsThisARestart:
        Surpluses = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
        Info_to_keep = yaml.load(open(filename_restart_file, 'r'), Loader=yaml.Loader)
        boundaries_coordinates = Info_to_keep['boundaries']
        internal_limits = Info_to_keep['internal_limits']
        coordinates_variations = Info_to_keep['coordinates_variations']
        alpha = Info_to_keep['alpha']
        active_index_set = []
        old_index_set = []
        #All next indexes are admissible at this stage and are active
        admissible_indexes = next_indexes(active_index_set, dimension)
        active_index_set.append(*admissible_indexes)


    elif not(IsThisARestart):
        #Keep some info in the restart dictionnary
        Info_to_keep = {}
        Info_to_keep.update({'boundaries': boundaries_coordinates})
        Info_to_keep.update({'internal_limits': internal_limits})
        Info_to_keep.update({'energy_threshold': energy_threshold})
        Info_to_keep.update({'LF': LagrangeFlag})
        Info_to_keep.update({'minimum_coordinates': minimum_coordinates})

        if LagrangeFlag:
            Info_to_keep.update({'b_type': b_type})
            Info_to_keep.update({'alpha': alpha})

        #Initialize dictionnary:
        Surpluses = {}

        #Initialize the grid indexes
        active_index_set = []
        old_index_set = []
        coordinates_variations = []
        #All next indexes are admissible at this stage and are active
        admissible_indexes = next_indexes(active_index_set, dimension)
        active_index_set.append(*admissible_indexes)

        #Points within the active_indexes, for a new job (within latest (el)if):
        #At this stage, there should only be the initial point but the procedure is kept as general as possible
        for active_index in active_index_set:  
            #We need to figure out what are the points that belong to such level index
            active_index_type = coordinates_type_processing(active_index)
            if not(active_index_type in coordinates_variations):
                coordinates_variations.append(active_index_type)
            level_index = np.array(active_index).astype(int)
            factor_error_domain_size = domain_level_size_processing(level_index, internal_limits)
            perm_list = []
            for j in range(dimension):
                for i in range(1, 2**level_index[j]):
                    if i % 2 != 0:
                        perm_list.append(i)
            for point_index in unique_lists(permutations(sorted(perm_list), dimension)):  
                position_point_nd = []
                for j in range(dimension):
                    spacing = 2.0**(-1.*level_index[j])
                    position_point = point_index[j]*spacing
                    position_point_nd.append(position_point)
                # We scale the position of the point to compute the function in the span of the real coordinates
                position_point_nd = position_processing(position_point_nd, internal_limits)
                value_function, trash = my_function_to_learn(position_point_nd, id1)
                if LagrangeFlag:
                    point_surplus = value_function 
                    point_error = value_function #* factor_error_domain_size 
                else:
                    point_surplus, point_error, trash = get_surplus_nd(value_function, level_index, point_index, Surpluses)  #The ID number is useless in this instance as points are not computed in parallel
                #The value of the function is added to the dictionnary for those points
                Surpluses.update({str(list(level_index))+str(list(point_index)): [point_surplus, point_error+1.E+4, value_function]})  #Surplus, error The first point error has to be very large otherwise the construction stops.
    #Compute the initial error
    if Verbose: print('Compute the initial residual error', flush=True)
    residual_error = error_of_a_set_nd(active_index_set, Surpluses, energy_threshold)  
    error_nd_indexes = [residual_error]
    if Verbose: print('Done computing the initial residual error', flush=True)

    Info_to_keep.update({'ActiveIndexSet' : active_index_set})
    Info_to_keep.update({'OldIndexSet' : old_index_set})
    Info_to_keep.update({'coordinates_variations': coordinates_variations})

    #Save all the work in files
    file_expansion = open(filename_expansion, "w")
    yaml.dump(Surpluses, file_expansion)
    file_expansion.close()

    file_restart = open(filename_restart_file, "w")
    yaml.dump(Info_to_keep, file_restart)
    file_restart.close()
    if LagrangeFlag:
        model = {}
        #print('start constructing the model')
        model = construct_model(Surpluses, Info_to_keep, n_workers, exe)
        #print('end of construction')
        file_model = open(filename_model, "w")
        yaml.dump(model, file_model)
        file_model.close()


    #We add new level indexes until the residual error reaches the target residual
    while any(error_nd_indexes[j] > error_threshold for j in range(len(error_nd_indexes))) or init == True: 
        init = False

        #Within the active indexes, find the one with the largest error
        current_index = active_index_set[np.argmax(error_nd_indexes)]
        print('Current index', current_index)
        if Verbose: print('current', current_index)

        #We remove the selected index from the active set to be put in the old set
        old_index_set.append(current_index)
        active_index_set.remove(current_index)

        #From this new current active index, obtain a list of all admissible new active_sets
        new_indexes = next_indexes(current_index, dimension)  #The last argument if the number of dimensions
    
        #Figure out which of these indexes are admissible and add them to the active set:
        admissible_indexes = []
        for index in new_indexes:
            level_type = level_type_processing(index) 
            if Verbose: print('parents of', index, 'are/is', parent_indexes(index,dimension))
            if level_type <= max_order:  #Indexes above the max_order are not deemed admissible
                if all(parent in old_index_set for parent in parent_indexes(index, dimension)) or SamplingMode:
                    admissible_indexes.append(index)
                    active_index_set.append(index)
        if Verbose : print('New active indexes:', admissible_indexes, flush=True)
    
        #Include the points of the admissible_indexes within the dictionnary
        latest_coordinates_variations = []
        points_of_this_iter = 0


        if not(DeltaFitting):
            if not(LagrangeFlag): 
                pot_func = potential_nd_func(Surpluses)
            else:
        #         model_func = lambda x: evaluate_model(x, model, Info_to_keep)
                if model == {}:
                    model_func = lambda x: 0.
                else:
                    primitive_model = collect_model(model, Surpluses.copy(), Info_to_keep)
                    model_func = lambda x: evaluate_primitive_model(x, primitive_model, Info_to_keep)

        for level_index in admissible_indexes:
            active_index_type = coordinates_type_processing(level_index)
            if not(active_index_type in coordinates_variations):
                coordinates_variations.append(active_index_type)
            latest_coordinates_variations.append(active_index_type)
            print('Working on', level_index, flush=True)
            level_index = np.array(level_index).astype(int)
            level_type = level_type_processing(level_index) 
            if Verbose: print('Computing new positions', flush=True)
            new_positions, points_indexes, points_in_dic = points_to_compute(level_index, dimension, internal_limits, boundaries_coordinates, spatial_adaptivity, error_threshold, Surpluses, SamplingMode) 
            #new_positions is in "real" space, not in the grid space [0,1].

            if Verbose: print(level_index, 'number of new points', len(new_positions), flush=True)
            #If this is a restart, we need to check if those points do not already exist in the dictionary (This is done in the points_to_compute subroutine)
            if Verbose: print('Done computing new positions', flush=True)
            #Compute the expected value of the potential at given point from the current approximation of the fit
            if level_type > 0 and any(j > 2 for j in level_index) and spatial_adaptivity and not(SamplingMode):
                if Verbose: print('Filtering high energy points', flush=True)
                #We define the lambda function to obtain the value of the potential
                points_below = []
                points_above = []
                index_below = []
                index_above = []
                for h in range(len(new_positions)):
                    if not(DeltaFitting): 
                        if LagrangeFlag:
                            expected_potential = model_func(new_positions[h])
                        else:
                            expected_potential = pot_func(position_processing_descale(new_positions[h], internal_limits, boundaries_coordinates))
                    if DeltaFitting: expected_potential = base_model(new_positions[h])
                    if expected_potential > energy_threshold: 
                        points_above.append(new_positions[h])
                        index_above.append(points_indexes[h])
                    else:
                        points_below.append(new_positions[h])
                        index_below.append(points_indexes[h])
                if len(points_above) > 0:
                    above_potentials = np.zeros(len(points_above))
                    for m in range(len(points_above)):
                        if not(DeltaFitting): 
                            if LagrangeFlag:
                                above_potentials[m], trash = min_domain_func_lagrange(points_above[m],1 , level_index = np.array(level_index), surplus_dict=Surpluses, internal_limits=internal_limits, boundaries_coordinates=boundaries_coordinates,error_threshold=error_threshold , potential_function=model_func)
                                #print(above_potentials[m], points_above[m][-1])
                            else:
                                above_potentials[m], trash = min_domain_func(position_processing_descale(points_above[m], internal_limits, boundaries_coordinates),1 , level_index = np.array(level_index), surplus_dict=Surpluses, internal_limits=internal_limits, boundaries_coordinates=boundaries_coordinates,error_threshold=error_threshold , potential_function=pot_func)
                        if DeltaFitting: above_potentials[m] = min_domain_base_model(position_x=points_above[m], level_index = np.array(level_index), surplus_dict=Surpluses, internal_limits=internal_limits, boundaries_coordinates=boundaries_coordinates)
                        if above_potentials[m] < energy_threshold:
                            points_below.append(points_above[m])
                            index_below.append(index_above[m])
                new_positions = np.array(points_below)
                points_indexes = np.array(index_below)
                if Verbose: print('Done filtering', flush=True)
            latest_new_positions = len(new_positions)
            if SamplingMode: total_number_of_points += latest_new_positions

            # The positions that are kept are computed:
            factor_error_domain_size = domain_level_size_processing(level_index, internal_limits)
            #print('index', level_index, factor_error_domain_size)
#            print('sending points in the pool')
            if SamplingMode:
                futures = [exe.submit(my_function_to_learn, new_positions[id1], id1) for id1 in range(len(new_positions))]

            elif LagrangeFlag:
                futures = [exe.submit(my_function_to_learn, new_positions[id1], id1) for id1 in range(len(new_positions))]
                for t in as_completed(futures):
                    #print('point completed')
                    value_function = t.result()[0]
                    value_id = t.result()[1]
                    #print('start evaluation model in pool')
                    error = value_function - model_func(new_positions[value_id]) #evaluate_model(new_positions[value_id], model, Info_to_keep) 
                    #print('done eval in pool')
                    error = error * factor_error_domain_size
                    surplus = 1.0
                    Surpluses.update({str(list(level_index))+str(list(points_indexes[value_id])): [surplus,  error, value_function]})
            else:
                futures = [exe.submit(my_function_to_learn, new_positions[id1], id1) for id1 in range(len(new_positions))]
                for t in as_completed(futures):
                    #print('point completed')
                    value_function = t.result()[0]
                    value_id = t.result()[1]                    
                    surplus, error, trash = get_surplus_nd(value_function, list(level_index), points_indexes[value_id], Surpluses)
                    error = error * factor_error_domain_size 
                    Surpluses.update({str(list(level_index))+str(list(points_indexes[value_id])): [surplus,  error, value_function]})
            if latest_new_positions > 0: 
                file_expansion = open(filename_expansion, "w")
                yaml.dump(Surpluses, file_expansion)
                file_expansion.close()

#            '''
            if LagrangeFlag and not(SamplingMode) and latest_new_positions > 0:
                #Before we re-construct the new model, we reoptimize the alpha values
                for dim in range(dimension):
                    term = [0]*dimension
                    if b_type[dim] == 'morse':
                        term[dim] = 1
                        if term in latest_coordinates_variations:
                            points_cut, trash = unique_points_term(term, Surpluses, Info_to_keep)
                            if len(points_cut[0]) < 100 and len(points_cut[0]) > 1: #We don't want to reoptimize alpha too many times
#                            new_alpha = minimize_Vandermondian(b_type[dim], minimum_coordinates[dim], alpha[dim], points_cut[0]) 
#                            new_alpha = smoothing_objective(b_type[dim], minimum_coordinates[dim], alpha, points_cut, boundaries_coordinates[dim], term, Info_to_keep, dim)  
                                new_alpha = smoothing_objective(b_type[dim], minimum_coordinates[dim], alpha, points_cut, internal_limits[dim], term, Info_to_keep, dim)  

#                            if alpha[dim] != 1. and abs(new_alpha[0]-alpha[dim])/alpha[dim] > 0.1: #We don't want the value of alpha to explode
#                            print(new_alpha)
                                '''
                                if abs(new_alpha[0]-alpha[dim])/alpha[dim] > 0.1: #We don't want the value of alpha to explode
                                    if new_alpha[0] > alpha[dim]:
                                        new_alpha[0] = alpha[dim]*1.05
                                    else:
                                        new_alpha[0] = alpha[dim]*0.95
                                '''
                                if alpha[dim] != float(new_alpha[0]) and not(term in latest_coordinates_variations): latest_coordinates_variations.append(term)
                                alpha[dim] = float(new_alpha[0])
                            #print(alpha)
                                Info_to_keep.update({'alpha': alpha})
#            '''

        if not SamplingMode:
            if latest_new_positions > 0:
                file_expansion = open(filename_expansion, "w")
                yaml.dump(Surpluses, file_expansion)
                file_expansion.close()
                if Verbose: print('adaptation of the model', flush=True)
                new_model = construct_model(Surpluses, Info_to_keep, n_workers, exe, adapt_model=True, old_model = model, adapted_terms = latest_coordinates_variations)
                model = new_model.copy()
                if Verbose: print('done adapting the model', flush=True)

            residual_error = error_of_a_set_nd(active_index_set, Surpluses, energy_threshold)
            #This is for the convergence of every single one of the 'cuts' to the given threshold
            error_nd_indexes = []
            for active_index in active_index_set:
                error_of_this_index, no_points, no_points_limit = error_measure_nd_active_set(np.array(active_index).astype(int), Surpluses, energy_threshold)
#            if no_points_limit > 0: 
                if no_points != 0:
                    error_nd_indexes.append(error_of_this_index)
                else:
                    error_nd_indexes.append(0.0)

        Info_to_keep.update({'ActiveIndexSet' : active_index_set})
        Info_to_keep.update({'OldIndexSet' : old_index_set})
        Info_to_keep.update({'coordinates_variations': coordinates_variations})

        #Save all the work in files
        file_expansion = open(filename_expansion, "w")
        yaml.dump(Surpluses, file_expansion)
        file_expansion.close()

        file_restart = open(filename_restart_file, "w")
        yaml.dump(Info_to_keep, file_restart)
        file_restart.close()

        if not os.path.isfile('./stop'):
            print("You asked me to stop maniac")
            exit()
        if total_number_of_points > sampled_points:
            print(f'At least, {sampled_points} points have been computed')
            exit()

    if not SamplingMode:
        if Verbose: print('last model construction', flush=True)
        model = construct_model(Surpluses, Info_to_keep, n_workers, exe)
        file_model = open(filename_model, "w")
        yaml.dump(model, file_model)
        file_model.close()


