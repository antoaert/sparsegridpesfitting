import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import ast
import sys
import os

from functools import partial

from itertools import permutations, product

from basis1dsrc import *
from active_indexes import *

from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from concurrent.futures import as_completed


from random import random
import time



def basisnd_carr(positions, l, j, b_type, internal_limits, x_ref, surplus_dict, **kwargs):
    """
    Generate a  nd basis based on a regular grid.

    Parameters:
    - l: the level indexes
    - j: the point indexes where we want the basis function to be centered 

    Returns:
    - 
    """
    grid_type=kwargs.get('GRID', 'regular')

    #We need to extract the relevant points to a given grid
    #For a 1D term, that is all the existing points of the existing 1D grid
    level_type = level_type_processing(l)
    i_kept = []
    j_kept = []
    if level_type == 0: #Just the constant
        i_kept=[1]
        j_kept=[1]
    if level_type == 1:
        current_index = OneD_level_index(l)
        for key in surplus_dict:
            level_index, point_index = ast.literal_eval(key.replace('][', '],['))
            if level_index[current_index]>1:
                i_kept.append(level_index[current_index]) #The points are double indexes! Grid level, point level (i,j)
                j_kept.append(point_index[current_index])
    functions = []
    for index in range(len(l)):
    # Check if l is valid
        if l[index] < 1:
            raise ValueError("The level can only be 1 or greater")
    #Check if we ask for a basis located at a child point
        if j[index] % 2 == 0:
           raise ValueError("The point you are specifying is an ancestor and not a child or is beyond the limit of the grid") 
        if j[index] > 2**l[index]:
           raise ValueError("This point is beyond the limit of the grid level")
        func1d = basis1d_carr(b_type[index], x_ref[index], int(l[index]), i_kept, j_kept, internal_limits[index])
        functions.append(func1d(position_processing([positions[index]], [internal_limits[index]]), j[index]))
    func = np.prod(functions)
    return func 

def basisnd(positions, l, j, **kwargs):
    """
    Generate a  nd basis based on a regular grid.

    Parameters:
    - l: the level indexes
    - j: the point indexes where we want the basis function to be centered 

    Returns:
    - 
    """
    grid_type=kwargs.get('GRID', 'regular')
    functions = []
    for index in range(len(l)):
    # Check if l is valid
        if l[index] < 1:
            raise ValueError("The level can only be 1 or greater")
    #Check if we ask for a basis located at a child point
        if j[index] % 2 == 0:
           raise ValueError("The point you are specifying is an ancestor and not a child or is beyond the limit of the grid") 
        if j[index] > 2**l[index]:
           raise ValueError("This point is beyond the limit of the grid level")
        func1d = basis1d(int(l[index]), int(j[index]))
        functions.append(func1d(positions[index]))
    func = np.prod(functions)
    return func 


'''
#Illustration of the nd functions 
basis_func = lambda *args : basisnd(np.array(*args), [3,1],[5,1])

bounds_figure=(0.,1.)


#génération de la figure de la base donnée
x_train=np.random.uniform(low=bounds_figure[0],high=bounds_figure[1],size=5000) 
y_train=np.random.uniform(low=bounds_figure[0],high=bounds_figure[1],size=5000) 

y_basis=np.zeros(len(x_train))
for xij in range(len(x_train)):
    y_basis[xij] = basis_func([x_train[xij],y_train[xij]])




fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_trisurf(x_train, y_train, y_basis, color='white', edgecolors='grey', alpha=0.5)
ax.scatter(x_train, y_train, y_basis, c='red')
plt.show()




exit()
'''

def integral_of_basisnd_carr(l, j, bounds, b_type, x_ref):
    integral=0.0
    for index in range(len(l)):
    # Check if l is valid
        if l[index] < 1:
            raise ValueError("The level can only be 1 or greater")
    #Check if we ask for a basis located at a child point
        if j[index] % 2 == 0:
           raise ValueError("The point you are specifying is an ancestor and not a child or is beyond the limit of the grid") 
        if j[index] > 2**l[index]:
           raise ValueError("This point is beyond the limit of the grid level")
        func1d = basis1d_carr(b_type[index], x_ref[index], int(l[index]), bounds[index])
        func1d_a = lambda x: func1d(x, j[index])
        integral_bounds = position_processing([2.**(-1.*l[index])*(j[index]-1), 2.**(-1.*l[index])*(j[index]+1)], bounds) 
        integral += integral_of_basis1d_bd(func1d_a, integral_bounds)[0]
    return integral 

def integral_of_basisnd(l, j):
    integral=0.0
    for index in range(len(l)):
    # Check if l is valid
        if l[index] < 1:
            raise ValueError("The level can only be 1 or greater")
    #Check if we ask for a basis located at a child point
        if j[index] % 2 == 0:
           raise ValueError("The point you are specifying is an ancestor and not a child or is beyond the limit of the grid") 
        if j[index] > 2**l[index]:
           raise ValueError("This point is beyond the limit of the grid level")
        func1d = basis1d(l[index], j[index])
        integral += integral_of_basis1d(func1d)[0]
    return integral 

#print(integral_of_basisnd([2,2], [1,1]))
#exit()
'''
def get_surplus_nd_carr(valf, l, j, surpluses, **kwargs):
    """
    valf : function value
    l    : level 
    j    : point number
    surpluses : dictionnary that contains the points surpluses
    """
    Verbose=kwargs.get('Verbose', False)
    id1 = kwargs.get('ID1', 99)
    LagrangeFlag = kwargs.get('LF', False)
    b_type = kwargs.get('b_type', [])
    boundaries = kwargs.get('boudaries', [])
    internal_limits = kwargs.get('internal_limits', [])
    minimum_coordinates = kwargs.get('minimum_coordinates', [])
    position_point_nd=[]
    constant = 0.0
    for index in range(len(l)):
        spacing=2.0**(-1.*l[index])
        position_point=j[index]*spacing
        position_point_nd.append(position_point)
    for key in surpluses:
        level_index, point_index = ast.literal_eval(key.replace('][', '],['))
        if level_index == [1]*len(level_index):
            constant = surplus_dict[key][0] 
    new_surplus = valf - float(constant)
    #We want to get the difference with the current state of the model potential
    error_estimated = new_surplus-potential_nd(position_point_nd, surpluses, internal_limits, boundaries, LF = LagrangeFlag, minimum_coordinates=minimum_coordinates, b_type = b_type)
    return (new_surplus, error_estimated, id1)
'''
def get_surplus_nd(valf, l, j, surpluses, **kwargs) :
    """
    valf : function value
    l    : level 
    j    : point number
    surpluses : dictionnary that contains the points surpluses
    """
    Verbose=kwargs.get('Verbose', False)
    id1 = kwargs.get('ID1', 99)

    #Index of the ancestors in each dimension
    ancestors_lists=[]
    position_point_nd=[]
    for index in range(len(l)):
        if j[index]>2**l[index] :
            raise ValueError("This point is beyond the limit of the grid level")

        spacing=2.0**(-1.*l[index])
        position_point=j[index]*spacing
        position_point_nd.append(position_point)
    #Figure out what are the parent points
        ancestors_pos=[0.5] #If want bounds --> add them here!!
        for h in range(2,l[index]):
           if position_point > ancestors_pos[-1]:
               ancestors_pos.append(ancestors_pos[-1]+2.0**(-1.*h))
           elif position_point < ancestors_pos[-1]:
               ancestors_pos.append(ancestors_pos[-1]-2.0**(-1.*h))
        ancestors_pos=np.array(ancestors_pos)

        ancestors=list()
        ancestors.append([1,1])
        for i in range(2, l[index]) : 
            ancestors.append([i, int(ancestors_pos[i-1]/(2.0**(-1.*i)))]) 
        #Need to include itself for each dimension
        ancestors.append([l[index], j[index]])
        ancestors_lists.append(ancestors) 
    ancestors_indexes = []
    new_surplus = valf
    if parent_indexes(l, len(l)) != []: #If == None, there is no parent...
        for el in parent_indexes(l, len(l)):
            ancestors_indexes.append(list(np.array(el).astype(int)))
        while not([1]*len(l) in ancestors_indexes):
            for index in ancestors_indexes:
                parents = parent_indexes(index, len(l))
                for el in parents:
                    ancestors_indexes.append(np.array(el).astype(int))
            ancestors_indexes = unique_lists(ancestors_indexes)
        #From the level indexes, we still need the point indexes
        points_indexes = []
        for ancestor_index in ancestors_indexes: 
            point_index = []
            for dim in range(len(l)):
                point_index.append(int(ancestors_lists[dim][int(ancestor_index[dim]-1)][1]))
            points_indexes.append(np.array(point_index))
    #The surplus is the difference between the value of the function and the value of the parent functions at that point. 
    
        for index in range(len(ancestors_indexes)) :
            ancestor_func = lambda *args : basisnd(np.array(*args), ancestors_indexes[index] , points_indexes[index])
            dict_entry=surpluses[str(list(np.array(ancestors_indexes[index]).astype(int)))+str(list(points_indexes[index]))]
            new_surplus=new_surplus-ancestor_func(position_point_nd)*dict_entry[0]
    error_estimated = np.abs(new_surplus*integral_of_basisnd(l, j))
    return (new_surplus, error_estimated, id1)




#I want to define my basis into a dictionnary
def constr_basis1d_carr_spatial(surplus_dict, restart_dict):
    #All the possibilities within a 1D cut are included in the 1D terms
    basis1d = {}
    coordinates_terms = restart_dict['coordinates_variations']
    dimension = len(coordinates_terms[0])
    internal_limits = restart_dict['internal_limits']
    b_type = restart_dict['b_type']
    alpha = restart_dict['alpha']
    minimum_coordinates = restart_dict['minimum_coordinates']
    for term in coordinates_terms:
        level_type = sum(term)
        if level_type == 1:
            dim = term.index(1)
            grid_level_index = [1]
            grid_point_index = [1]
            positions_of_this_term = [minimum_coordinates] #We add the reference point in all dimensions
            for key in surplus_dict:
                level_index, points_index = ast.literal_eval(key.replace('][', '],['))
                if term == coordinates_type_processing(level_index):
                    position_point_nd = []
                    for index in range(dimension):
                        spacing=2.0**(-1.*level_index[index])
                        position_point=points_index[index]*spacing
                        position_point_nd.append(position_point)
                    positions_of_this_term.append(position_processing(position_point_nd, internal_limits))
                    grid_level_index.append(level_index[dim])
                    grid_point_index.append(points_index[dim])
            positions_of_this_term=np.array(positions_of_this_term)
            basis_coeff = basis1d_carr_spatial(alpha[dim], b_type[dim], minimum_coordinates[dim], positions_of_this_term[:, dim])
            for j in range(len(basis_coeff)):
                basis1d.update({str([dim, grid_level_index[j], grid_point_index[j]]) : basis_coeff[:,j]})
    return basis1d

def basis1d_eval(b_type, x_ref, coeffs, alpha):
    """ b_type: basis type , e.g. 'Morse', 'polynomial' ...
        x_ref: position of reference (chosen to be the reference point of the interpolation)
    """
    if b_type == 'polynomial':
        primitive_function = lambda x, x_ref, k : (x-x_ref)**(k+1)
    if b_type == 'morse':
        primitive_function = lambda x, x_ref, k : (1.-np.exp(-1.0*alpha*(x-x_ref)))**(k+1)
    if b_type == 'trigonometric':
        primitive_function = lambda x, x_ref, k : (np.sin(int((k+2)/2)*(x-x_ref))) if k % 2 != 0 else (2.*(np.sin(0.5*(int((k+2)/2)*(x-x_ref))))**2.) #int() behaves like math.floor()
        #if k starts at zero, the two first primitives would be constants, which makes the interpolation impossible

    npoints = len(coeffs)
    lagrange_func = lambda x : sum(coeffs[j]*primitive_function(x, x_ref, j) for j in range(npoints)) 
    return lagrange_func

def unique_points_term(term, surplus_dict, restart_dict):
    dimension = len(term)
    internal_limits = restart_dict['internal_limits']
    b_type = restart_dict['b_type']
    minimum_coordinates = restart_dict['minimum_coordinates']
    unique_points = [[] for i in range(sum(term))] 
    unique_points_indexes = [[] for i in range(sum(term))] 
    t = 0 #We add the minimum coordinates to the points of the basis
#    for index in range(dimension):
#        if term[index] == 1:
#            if b_type[index] != 'trigonometric': 
#                unique_points[t].append(minimum_coordinates[index])
#                unique_points_indexes[t].append([1,1])
#            t += 1
    for key in surplus_dict:
        level_index, points_index = ast.literal_eval(key.replace('][', '],['))
        if term == coordinates_type_processing(level_index):
            position_point_nd = []
            for index in range(dimension):
                spacing=2.0**(-1.*level_index[index])
                position_point = points_index[index]*spacing
                position_point_nd.append(position_point)
            position_point_nd = position_processing(position_point_nd, internal_limits) #In the "real" space
            t = 0
            for index in range(dimension):
                if term[index] == 1:
                    if not(position_point_nd[index] in unique_points[t]):
                        unique_points[t].append(position_point_nd[index])
                        unique_points_indexes[t].append([level_index[index], points_index[index]])
                    t += 1
    return unique_points, unique_points_indexes 

def basis_term(term, unique_points, restart_dict):
    dimension = len(term)
    if not(term in restart_dict['coordinates_variations']):
        print('this term does not exist')
        exit()

    internal_limits = restart_dict['internal_limits']
    b_type = restart_dict['b_type']
    alpha = restart_dict['alpha']
    minimum_coordinates = restart_dict['minimum_coordinates']
    coefficients = []
    t=0
    for index in range(dimension):
        if term[index] == 1:
#            print(unique_points[t])
            coefficients.append(basis1d_carr_spatial(alpha[index], b_type[index], minimum_coordinates[index], unique_points[t]))
            t += 1
            #Coefficients are given by indexes: dimension, order of the primitive
            #The position of the related point in its dimension is given by the unique_points vector
    return coefficients


def construct_model(surplus_dict, restart_dict, n_workers, exe, **kwargs):
    adapt_model = kwargs.get('adapt_model', False) #Can be used if one doesnt want to run the entire construction again, in that case, a previous version of the model must be provided, as well as the terms that have been adapted
    old_model = kwargs.get('old_model', {})
    adapted_terms = kwargs.get('adapted_terms', [])

    unique_points_dict = {}
    unique_levels_dict = {}
    model_new = {}
    model = {}
    terms = restart_dict['coordinates_variations']
    minimum_coordinates = restart_dict['minimum_coordinates']
    dimension = len(terms[0])
    internal_limits = restart_dict['internal_limits']
    b_type = restart_dict['b_type']

#    exe=ThreadPoolExecutor(max_workers=n_workers)
    if adapt_model: #In principle, since the terms are evaluated in order, there should be no influence from the old version of the model on the important parts of the model that are adapted
        terms_kept = []
        for term in terms:
            if term in adapted_terms:
                terms_kept.append(term)
            elif any(is_it_a_child_term(term, terma) for terma in adapted_terms):
                terms_kept.append(term)
        terms = terms_kept
        model_new = old_model.copy()
        for key in old_model:
            level_index, points_index = ast.literal_eval(key.replace('][', '],['))
            term = coordinates_type_processing(level_index)
            if term in terms:
                model_new.pop(key)
        model = model_new.copy()
#        model_new = old_model.copy()
    #There is an issue, as we don't take into account that our basis should be zero at lower dimensional points.
    #The best way to treat it, is to work with the list of unique points, if one combination does not exist, it means the point was never computed (this is in nD with n>1), or that the point belongs to a lower dimensional grid. And therefore should be treated as being a zero correction.
    points_to_term = lambda x: unique_points_term(x, surplus_dict, restart_dict)
    futures = [exe.submit(points_to_term, terms[value_id]) for value_id in range(len(terms))]
    t = 0
    for elem in futures:
        unique_points_dict.update({str(terms[t]): elem.result()[0]})
        unique_levels_dict.update({str(terms[t]): elem.result()[1]})
        t += 1
    for term in terms:  #The terms need to be processed in order! Or else the nD terms are not corrections to the lower n terms
        #unique_points, unique_points_index = unique_points_term(term, surplus_dict, restart_dict)
        unique_points = unique_points_dict[str(term)]
        unique_points_index = unique_levels_dict[str(term)]
        #We get all combinations of the indexes
        indexes = [] 
        for j in range(sum(term)):
            indexes.append(range(len(unique_points[j])))
        all_combinations = list(itertools.product(*indexes))
        
#        model_func = lambda x: evaluate_model(x, model_new, restart_dict)
        primitive_model = collect_model(model_new, surplus_dict, restart_dict)
        model_func = lambda x: evaluate_primitive_model(x, primitive_model, restart_dict)
        combination_func = lambda x, y: combination_to_point(x, y, term, unique_points_index, dimension, internal_limits, model_func)
#        for combination in all_combinations:
        futures1 = [exe.submit(combination_func, all_combinations[id1], id1) for id1 in range(len(all_combinations))]
        for t in futures1:
            key = t.result()[1]
            model_value = t.result()[2]
#            position_point_nd, key, model_value = combination_func(combination)
            if not(key in model):
                if key in surplus_dict: #Check that the point exists
                    surplus = surplus_dict[key][2] - model_value 
                else:
                    surplus = 0.0
                model.update({key : surplus})
            #We collect the coefficients to obtain the coefficients in the base of the initial primitives basis
        model_new = model.copy()
    return model

def evaluate_model(position_x, model, restart_dict):
    b_type = restart_dict['b_type']
    alpha = restart_dict['alpha']
    minimum_coordinates = restart_dict['minimum_coordinates']
    internal_limits = restart_dict['internal_limits']
    model_value = 0.0
    dimension = len(position_x)
    treated_terms = []
    unique_points = []
    basis = []
    for key in model:
        level_index, points_index = ast.literal_eval(key.replace('][', '],['))
        d_coeff = model[key]
        term = coordinates_type_processing(level_index)
        if term in treated_terms:
            j = treated_terms.index(term)
            points = unique_points[j]
            coeff_basis = basis[j]
        else:
            points, trash = unique_points_term(term, model, restart_dict)
            treated_terms.append(term)
            unique_points.append(points)
            coeff_basis = basis_term(term, points, restart_dict)
            basis.append(coeff_basis)
#        coeff_basis = basis_term(term, points, restart_dict)
        position_point_nd = []
        for index in range(dimension):
            spacing=2.0**(-1.*level_index[index])
            position_point = points_index[index]*spacing
            position_point_nd.append(position_point)
        position_point_nd = position_processing(position_point_nd, internal_limits) #In the "real" space
        product = 1.0
        t = 0
        for index in range(dimension):
            if term[index] == 1:
                n_order = points[t].index(position_point_nd[index])
                func1d = basis1d_eval(b_type[index], minimum_coordinates[index], coeff_basis[t][:,n_order], alpha[index])
                product = product * func1d(position_x[index])
                t += 1
        model_value += d_coeff*product
    return model_value

def combination_to_point(combination, id1, term, unique_points_index, dimension, internal_limits, model_func):
     #We need to figure out the level_index and point_index of this point ..
    point_index = []
    level_index = []
    i = 0
    for dim in range(dimension):
        if term[dim] == 1:
            level_index.append(unique_points_index[i][combination[i]][0])
            point_index.append(unique_points_index[i][combination[i]][1])
            i += 1
        else:
            level_index.append(1)
            point_index.append(1)
    position_point_nd = []
    for index in range(dimension):
        spacing=2.0**(-1.*level_index[index])
        position_point = point_index[index]*spacing
        position_point_nd.append(position_point)
    position_point_nd = position_processing(position_point_nd, internal_limits) #In the "real" space
    key1 = str(list(level_index))+str(list(point_index))
    return position_point_nd, key1, model_func(position_point_nd), id1 
      


    


         




def collect_model(model, surplus_dict, restart_dict):
    primitive_model = {}
    terms = restart_dict['coordinates_variations']
    minimum_coordinates = restart_dict['minimum_coordinates']
    dimension = len(terms[0])
    unique_points_1d = []
    term = [0]*dimension
    for i in range(dimension):
        term[i] = 1
        if term in terms:
            unique_points_1d_i, trash = unique_points_term(term, surplus_dict, restart_dict)
            unique_points_1d.append(unique_points_1d_i[0])
        term[i] = 0
    unique_points_1d_r = []
    if len(unique_points_1d) == dimension: 
        for i in range(dimension):
            r = [ round(elem, 12) for elem in unique_points_1d[i] ]
            unique_points_1d_r.append(r)

    internal_limits = restart_dict['internal_limits']
    b_type = restart_dict['b_type']
    for term in terms:
#        print('**** term ****', term)
        unique_points, unique_points_index = unique_points_term(term, surplus_dict, restart_dict)
        point_primitive = []
        coeff_basis = basis_term(term, unique_points, restart_dict)
        #We get all combinations of the indexes
        indexes = []
        unique_points_r = []
        for j in range(sum(term)):
            indexes.append(range(len(unique_points[j])))
            r = [ round(elem, 12) for elem in unique_points[j]]
            unique_points_r.append(r)
        t = 0
        for j in range(dimension):
            if term[j] == 1:
                p = [ unique_points_1d_r[j].index(round(point,12)) for point in unique_points_r[t] ]
                point_primitive.append(p)
                t += 1
        #The combination of indexes are associated with specific points: 
        all_combinations = list(itertools.product(*indexes))
        k = 0
        for combination_primitive in all_combinations:
#            sumk = [0.0]*sum(term)
            sumk = 0.0 
            coeff = 0.0
            combination_point_1d_primitive = []
            t = 0
            for dim in range(dimension):
                if term[dim] == 1:
                    combination_point_1d_primitive.append(point_primitive[t][combination_primitive[t]])
                    t += 1
            for combination_lagrange in all_combinations:
                #We need to figure out the level_index and point_index of this point ...
                point_index = []
                level_index = []
                i = 0
                for dim in range(dimension):
                    if term[dim] == 1:
                        level_index.append(unique_points_index[i][combination_lagrange[i]][0])
                        point_index.append(unique_points_index[i][combination_lagrange[i]][1])
                        i += 1
                    else:
                        level_index.append(1)
                        point_index.append(1)
                if level_type_processing(level_index) == sum(term): 
                    position_point_nd = []
                    for index in range(dimension):
                        spacing=2.0**(-1.*level_index[index])
                        position_point = point_index[index]*spacing
                        position_point_nd.append(position_point)
                    position_point_nd = position_processing(position_point_nd, internal_limits) #In the "real" space
                    key = str(list(level_index))+str(list(point_index))
#                    print('looking for point', key)
                    coeff_lagrange = 0.0
                    if key in surplus_dict: #Check that the point exist
                        if key in model:
                            coeff_lagrange = model[key]
#                print(combination_lagrange, coeff_lagrange)
                    t = 0
                    prodk = 1.0
                    for i in range(dimension):
                        if term[i] == 1:
                            point_order = unique_points_r[t].index(round(position_point_nd[i],12))
                            prodk = prodk * coeff_basis[t][combination_primitive[t],point_order]
#                            sumk[t] += coeff_basis[t][combination_primitive[t],point_order]*abs(coeff_lagrange)**(1/sum(term))*sign(coeff_lagrange) 
                            t += 1
                    sumk += prodk * coeff_lagrange
            if sum(term) == 0:  #The first constant
                key = str(list([1]*dimension))+str(list([1]*dimension))
                coeff = 0.
                if key in model:
                    coeff = model[key]
            else:
                coeff = np.prod(sumk)
#            if sum(term)==1: 
            if coeff != 0.0:
                key = str(term)+str(list(combination_primitive))+str(list(combination_point_1d_primitive))
                primitive_model.update({key: float(coeff)})
#                print(term, list(combination_primitive), str(combination_point_1d_primitive), coeff)
    return primitive_model
def sign(num):
    return -1 if num < 0 else 1








def potential_nd_carr_spatial(position_x, surplus_dict, restart_dict):
    coordinates_terms = restart_dict['coordinates_variations']
    internal_limits = restart_dict['internal_limits']
    dimension = len(coordinates_terms[0])
    for term in coordinates_terms:
        positions_of_this_term = []
        values_function = []
        for key in surplus_dict:
            level_index, points_index = ast.literal_eval(key.replace('][', '],['))
            if term == coordinates_type_processing(level_index):
                position_point_nd=[]
                for index in range(dimension):
                    spacing=2.0**(-1.*level_index[index])
                    position_point=points_index[index]*spacing
                    position_point_nd.append(position_point)
                positions_of_this_term.append(position_processing(position_point_nd, internal_limits))
                values_function.append(surplus_dict[key][2])
                #We populate these two lists in the same order!
        #Now that we looped over all the given points, we can construct the basis for this set of points in whatever cut
        basis_functions = []
        for i in range(dimension):
            if term[i] == 1:
               print('not finished')            
        for i in range(len(values_function)):
            print('not finished')




def potential_nd(position_x, surplus_dict, internal_limits, boundaries_coordinates, **kwargs):
    LagrangeFlag = kwargs.get('LF', False)
    minimum_coordinates = kwargs.get('minimum_coordinates', [])
    b_type = kwargs.get('b_type', [])
    function_value = 0.0
    position = np.zeros(len(position_x))
    for dim in range(len(position_x)):
        factor_scaling = (internal_limits[dim][1]-internal_limits[dim][0])
        shifting_scaling = internal_limits[dim][0]
        position[dim] = (position_x[dim]-shifting_scaling)/factor_scaling 
    for key in surplus_dict:
        #For all existing points in the dictionnary, we need to obtain from the key value, the level index and point index
        level_index, point_index = ast.literal_eval(key.replace('][', '],['))
        surplus = surplus_dict[key][0]
        if LagrangeFlag:
            func = lambda *args : basisnd_carr(np.array(*args), level_index , point_index, b_type,  internal_limits, minimum_coordinates, surplus_dict)
            function_value += surplus*func(position_x) 
        else:
            func = lambda *args : basisnd(np.array(*args), level_index , point_index)
            function_value += surplus*func(position) 
    return function_value 

def geo_mean(iterable):
    a = np.array(iterable)
    return a.prod()**(1.0/len(a))

def key_processing_surplus(key, surplus_dict):
#    level_index, point_index = ast.literal_eval(key.replace('][', '],['))
    surplus = surplus_dict[key][0]
    return surplus

def key_processing_func(key):
    level_index, point_index = ast.literal_eval(key.replace('][', '],['))
    return lambda *args : basisnd(np.array(*args), level_index , point_index)


def position_processing(position_x, internal_limits):
    position = np.zeros(len(position_x))
    for dim in range(len(position_x)):
        factor_scaling = (internal_limits[dim][1]-internal_limits[dim][0])
        shifting_scaling = internal_limits[dim][0]
        position[dim] = (position_x[dim]*factor_scaling)+shifting_scaling
    return position

def position_processing_descale(position_x, internal_limits, boundaries_coordinates):
    position = np.zeros(len(position_x))
    for dim in range(len(position_x)):
        factor_scaling = (internal_limits[dim][1]-internal_limits[dim][0])
        shifting_scaling = internal_limits[dim][0]
        position[dim] = (position_x[dim]-shifting_scaling)/factor_scaling
    return position

def level_type_processing(level_index):
    level_type = 0
    for index in level_index:
        if index > 1:
            level_type += 1
    return level_type

def coordinates_type_processing(active_index):
    coord_type = []
    for i in range(len(active_index)):
        if active_index[i] > 1:
            coord_type.append(1)
        else:
            coord_type.append(0)
    return coord_type

def OneD_level_index(level_index):
    for index in level_index:
        if index>1:
            the_index=index
    return the_index

def level_number_points_processing(level_index):
    number_points = 0
    for index in level_index:
        if index > 1:
            number_points += 2**(index-1) 
    return number_points 

def domain_level_size_processing(level_index, internal_limits):
    factor = 1.
    for i in range(len(level_index)):
        if level_index[i] > 1:
            factor = factor * (internal_limits[i][1]-internal_limits[i][0])
    return factor 

def potential_nd_func(surplus_dict):#, internal_limits, boundaries_coordinates):
    return lambda *args: sum(key_processing_surplus(key, surplus_dict)*key_processing_func(key)(*args) for key in surplus_dict)


def evaluate_primitive_model(position_x, primitive_model, restart_dict):
    b_type = restart_dict['b_type']
    alpha = restart_dict['alpha']
    minimum_coordinates = restart_dict['minimum_coordinates']
    internal_limits = restart_dict['internal_limits']
    model_value = 0.0
    dimension = len(position_x)
    treated_terms = []
    unique_points = []
    basis = []
    for key in primitive_model:
        term, exponent_orders, point_index_in_1D_cut = ast.literal_eval(key.replace('][', '],[')) 
        d_coeff = primitive_model[key]
        product = 1.0
        t = 0
        for index in range(dimension):
            if term[index] == 1:
                if b_type[index] == 'polynomial':
                    primitive_function = lambda x, x_ref, k : (x-x_ref)**(k+1)
                elif b_type[index] == 'morse':
                    alphaa = alpha[index]
                    primitive_function = lambda x, x_ref, k : (1.-np.exp(-1.0*alphaa*(x-x_ref)))**(k+1)
                elif b_type[index] == 'trigonometric':
                    primitive_function = lambda x, x_ref, k : (np.sin(int((k+2)/2)*(x-x_ref))) if k % 2 != 0 else (2.*(np.sin(0.5*(int((k+2)/2)*(x-x_ref))))**2.) #int() behaves like math.floor()
                product = product * primitive_function(position_x[index], minimum_coordinates[index], exponent_orders[t]) 
                t += 1
        model_value += d_coeff*product
    return model_value


#potential_nd([1.,2.], [3,3], Surpluses)


def error_measure_nd(level_index, surplus_dict, energy_threshold):
    error_estimate = 0.0
    npoints        = 0
    npoints_below  = 0
#    print(level_index, 'coucou')
    dimension = len(level_index)
    for key in surplus_dict:
        #For all existing points in the dictionnary, we need to obtain from the key value, the level index and point index
        level_index_dict, point_index_dict = ast.literal_eval(key.replace('][', '],['))
        error = surplus_dict[key][1]
        valf  = surplus_dict[key][2]
        if level_index_dict == list(level_index):
            npoints        += 1
            factor          = 0.5 - 0.5 * np.tanh((valf-energy_threshold)*60.)
            error_estimate += error*factor
#            if valf <= energy_threshold: npoints_below += 1
    return error_estimate, npoints#, npoints_below

def error_measure_rmse(level_index, surplus_dict, energy_threshold):
    error_estimate = 0.0
    npoints = 0
    dimension = len(level_index)
    for key in surplus_dict:
        #For all existing points in the dictionnary, we need to obtain from the key value, the level index and point index
        level_index_dict, point_index_dict = ast.literal_eval(key.replace('][', '],['))
        error = surplus_dict[key][0]
        valf  = surplus_dict[key][2]
        if level_index_dict == list(level_index):
            npoints += 1
            factor = 0.5 - 0.5 * np.tanh((valf-energy_threshold)*60.)
            error_estimate += (error*factor)**2.
    sum_squares = error_estimate
    return sum_squares, npoints 



def error_measure_rmse_active_set(level_index, surplus_dict, energy_threshold):
    error_estimate = 0.0
    npoints = 0
    npoints_below_limit = 0
    dimension = len(level_index)
    for key in surplus_dict:
        #For all existing points in the dictionnary, we need to obtain from the key value, the level index and point index
        level_index_dict, point_index_dict = ast.literal_eval(key.replace('][', '],['))
        error = surplus_dict[key][0]
        valf  = surplus_dict[key][2]
        if level_index_dict == list(level_index):# and (valf <= energy_threshold or level_type < 2):
            #The error is multiplied with an energy-dependent factor that is of the form of a hyperbolic tangent.
            factor = 0.5 - 0.5 * np.tanh((valf-energy_threshold)*60.)
            if factor > 0.: npoints += 1 #The point has to contribute to the error 
            if valf < energy_threshold: npoints_below_limit += 1 
            error_estimate += (error*factor)**2.
    sum_squares = error_estimate
    return sum_squares, npoints, npoints_below_limit
def error_measure_nd_active_set(level_index, surplus_dict, energy_threshold):
    error_estimate = 0.0
    npoints = 0
    npoints_below_limit = 0
    dimension = len(level_index)
    for key in surplus_dict:
        #For all existing points in the dictionnary, we need to obtain from the key value, the level index and point index
        level_index_dict, point_index_dict = ast.literal_eval(key.replace('][', '],['))
        error = surplus_dict[key][1]
        valf  = surplus_dict[key][2]
        level_type = 0
#        for index in level_index_dict:
#            if index > 1:
#               level_type += 1
        if level_index_dict == list(level_index):# and (valf <= energy_threshold or level_type < 2):
            #The error is multiplied with an energy-dependent factor that is of the form of a hyperbolic tangent.
            factor = 0.5 - 0.5 * np.tanh((valf-energy_threshold)*60.)
            if factor > 0.: npoints += 1 #The point has to contribute to the error 
            if valf < energy_threshold: npoints_below_limit += 1 
            error_estimate += abs(error*factor)
    
    return error_estimate, npoints, npoints_below_limit

def error_of_a_set_nd(active_set, surplus_dict, energy_threshold, **kwargs):
    Verbose = kwargs.get('Verbose', False)
    error         = 0.0
#    npoints       = 0
#    npoints_below = 0
    for index_set in active_set:
        index = np.array(index_set).astype(int)
        error1, npoints1 = error_measure_nd(index, surplus_dict, energy_threshold)
        error         += error1
    return error/len(active_set)

def error_of_a_set_rmse(active_set ,surplus_dict, energy_threshold, **kwargs):
    Verbose=kwargs.get('Verbose', False)
    error = 0.0
    n_points = 0 
    for index_set in active_set:
        index = np.array(index_set).astype(int)
        error1, n_points1 = error_measure_rmse(index, surplus_dict, energy_threshold)
        error += error1
        n_points += n_points1 
        if Verbose: print(index_set, 'error', np.sqrt(error1/(n_points1+1)), n_points1)
    if n_points == 0: n_points += 1
    rmse = np.sqrt(error/n_points) 
    return rmse

def is_it_a_child_term(term1, term2): #Is term1 a child of term2?
    if term1 == term2:
        return True
    if sum(term1) > sum(term2):
        for i in range(len(term1)):
            if term1[i] == term2[i]:
                return True
    else:
        return False

#Values of the function are obtained from this one for now:
#def target_func(x) :
#    Slow = False
#    if Slow:
#        toto = 0.0
#        value = int(random() * 500)
#        for i in range(value):
#            for j in range(value):
#                for k in range(value):
#                    toto += float(i) * float(j) * float(k)
#    print(toto)
#    return(-75.0087+0.35*(1.-np.exp(-0.95*(x[0]-0.54)))**2.0+.35*(1.-np.exp(-0.95*(x[1]-0.54)))**2.+0.05*x[1]**2.*x[0])#+0.09*(x[2]-0.9)**2.)
#    return ((-1.99*np.cos(1-x*6.)*(x)))
#    return (x-0.5)**2.#+1.23
def target_func_id(x, id):
    Slow = False
    if Slow:
        toto = 0.0
        value = int(random() * 500)
        for i in range(value):
            for j in range(value):
                for k in range(value):
                    toto += float(i) * float(j) * float(k)
#    print(toto)
    return(-75.0087+0.35*(1.-np.exp(-0.95*(x[0]-1.14)))**2.0+.35*(1.-np.exp(-0.95*(x[1]-1.54)))**2.+0.005*x[1]**2.*x[0], id)#+0.09*(x[2]-0.9)**2.)



def task(task_id, pipi, **kwargs):
    pl = kwargs.get('PL', 1)
    # Simulate some work...
    toto = 0.0
    value = int(random() * 500)
    for i in range(value):
        for j in range(value):
            for k in range(value):
                toto += float(i) * float(j) * float(k)

  # return a result
    return (task_id, toto, pipi, pl)


def Pool_points(target_func, new_positions):
    with ProcessPoolExecutor() as exe:
        futures = [exe.submit(target_func, new_positions[id1]) for id1 in range(len(new_positions))]
        for t in as_completed(futures):
            print(t.result())

