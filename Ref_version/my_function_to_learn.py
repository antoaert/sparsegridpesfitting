#!bin/python
import os
import numpy as np
import subprocess
import uuid
from jinja2 import Template
import yaml
import math

from constants import *

def base_model(xy):
    ''' Units are atomic units of energy, rad, and bohr'''
    ''' Model potential (harmonic) from Mol. Phys. 54, 23 (1985)'''
    geometry = xy

    r1 = geometry[2]
    r2 = geometry[0]
    r3 = geometry[1]
    t1 = geometry[4]
    t2 = geometry[3]
    p1 = geometry[5]

    F11 = 0.4678897718/2.
    F22 = 0.8372561517/2.
    F23 = 0.1563272072/2.
    F24 = 0.0160209285/2.
    F25 = 0.0902997792/2.
    F33 = 0.1745675223/2.
    F34 = 0.032405969/2.
    F35 = 0.05607325/2.
    F44 = 0.1736238532/2.
    F45 = 0.06811926606/2.
    F55 = 0.5094036697/2.
    F66 = 0.03066513761/2.
    pot = 0.0
    
#    Q1 = (r1 - 1.85571486)
#    Q2 = (r2 - 2.23933)
#    Q3 = (r3 - 2.630504)
#    Q4 = (math.acos(t1)-1.815142)
#    Q5 = (math.acos(t2)-1.982694)
#    Q6 = (p1 - math.pi)
#Adjust minimum to high level method's minimum

    Q1 = (r1 -        1.7835716705)
    Q2 = (r2 -        2.1589035095)
    Q3 = (r3 -        2.5408722551)
    Q4 = (math.acos(t1)-1.8395)
    Q5 = (math.acos(t2)-1.9507)
    Q6 = (p1 - math.pi)


    pot += F11 * math.pow(Q1, 2)
    pot += F22 * math.pow(Q2, 2)
    pot += F23 * Q2 * Q3
    pot += F24 * Q2 * Q4
    pot += F25 * Q2 * Q5
    pot += F33 * math.pow(Q3, 2)
    pot += F34 * Q3 * Q4
    pot += F35 * Q3 * Q5
    pot += F44 * math.pow(Q4, 2)
    pot += F45 * Q4 * Q5
    pot += F55 * math.pow(Q5, 2)
    pot += F66 * math.pow(Q6, 2) 
    return pot
    


def my_function_to_learn(xy, id1): #MOLPRO ND
    data_base_folder=str(os.getcwd())+"/"+"computed_points/"
    if not os.path.exists(data_base_folder):
      os.mkdir(data_base_folder)
    geometry = xy
    #The procedure will provide the lengths in Bohr, molpro will use them in angstrom
    geometry_mapped=np.zeros(len(geometry))
    geometry_mapped[0]=geometry[0]/angstrom_to_bohr
    geometry_mapped[1]=geometry[1]/angstrom_to_bohr
    geometry_mapped[2]=geometry[2]/angstrom_to_bohr
    geometry_mapped[3]=np.arccos(geometry[3])/radian_per_degree
    geometry_mapped[4]=np.arccos(geometry[4])/radian_per_degree
    geometry_mapped[5]=geometry[5]/radian_per_degree

    #Use this list that define the geometry in the coordinates (and units) of the z-matrix in a template of the molpro calculation. 
    template_molpro_hono='''***, HONO aug-cc-pVTZ-F12 uks
    memory,250,m   
    print orbitals
    geometry={angstrom
    N 
    O 1 RO1N  
    O 1 RO2N 2 ANOO 
    H 3 RO2H 1 AHON 2 D}
   
    !basis=cc-pVTZ
    basis=cc-pVTZ
    RO1N={{r_o1n}}
    RO2N={{r_o2n}}
    RO2H={{r_o2h}}
    ANOO={{a_noo}}
    AHON={{a_hon}}
    D={{d_hono}}
    hf;
!    escf=energy
!    ccsd(t);
    eccsdt=energy!(1)
    !eccsdtb=energy(2)
    '''
    tm=Template(template_molpro_hono)

    input_file=tm.render(
    r_o1n=geometry_mapped[0],
    r_o2n=geometry_mapped[1],
    r_o2h=geometry_mapped[2],
    a_noo=geometry_mapped[3],
    a_hon=geometry_mapped[4],
    d_hono=geometry_mapped[5])

    #print(input_file)
    unique_filename = str(uuid.uuid4())
    directory_path=str(os.getcwd())+"/molpro_computations/"+"hono"+unique_filename+"/"
    directory_path1=str(os.getcwd())+"/molpro_computations"
    if not os.path.exists(directory_path1):
        os.mkdir(directory_path1)
    
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)

    input_file_path=directory_path+"hono"+".inp"
    with open(input_file_path, 'w') as f:
         print(input_file, file=f)
    f.close()

    #Once the input file is created, run the program
    exit_status=os.system("cd "+str(directory_path)+ " && " + "/opt/Molpro/2024.1.0/molpro_2024.1/bin/molpro -n 4 hono.inp --no-xml-output -d /scratch/antoaert")  #+str(input_file_path))
  
    if exit_status!=0:
        print("The molpro command failed")
        exit()
    else:
        #Once this is over, get what you are interested in from the output
        proc = subprocess.Popen('grep -r "SETTING ECCSDT" '+str(directory_path)+'''hono.out | head -1 | cut -d "=" -f 2 | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
        #proc = subprocess.Popen("awk '/SETTING ESCF/{print $4;exit}' "+str(directory_path)+'''hono.out''', stdout=subprocess.PIPE, shell=True)
        energy=proc.communicate()[0]
        #Keep it somewhere
        path_file_db=data_base_folder+"hono_tab.txt"
        newline=''  #str(current_term).replace(' ','')+' '
        for i in range(len(geometry_mapped)):
          newline+="{:.8f}".format(geometry_mapped[i])+' '
        newline+="{}".format(float(energy))+' '
        print(newline,file=open(path_file_db,"a"))
    energy = float(energy) - float(base_model(geometry))

    return (float(energy), id1)
