import sys
sys.path.append('Source/')
from construct_dict import *
from basis1dsrc import *
from basisnd import *
from constants import *
from grid1d import *
from my_function_to_learn import *
from points_to_be_computed import *


dimension              = 6
n_workers              = 4  #number of maximum concurrent points to be computed
error_threshold        = 1e-04
spatial_adaptivity     = True
IsThisARestart         = False
Verbose                = False
DeltaFitting           = False#True
LagrangeFlag           = True

SamplingMode           = False  #Cannot be combined with spatial_adaptivity!
sampled_points         = 100    #Only use in Sampling Mode

filename_expansion     = 'HONO'    
filename_restart_file  = 'restart'
filename_model         = 'model'
boundaries_coordinates = [(1.90,2.60),(2.1,3.25),(1.3,2.45),(-0.65,-0.1),(-0.65,0.25),(3.1415/2., 3.1415*3./2.)]
minimum_coordinates    = [2.1589035095, 2.5408722551, 1.7835716705, math.cos(1.9507), math.cos(1.8395), math.pi] #Minimum HF
#The b_type list will not influence the construction if the LagrageFlag is 'False'
b_type                 = ['morse', 'morse', 'morse', 'polynomial', 'polynomial', 'polynomial']
b_type                 = ['polynomial', 'polynomial', 'polynomial', 'polynomial', 'polynomial', 'polynomial']


max_order              = 1
energy_threshold       = 20000./219474.





if __name__ == '__main__':
    main(dimension, n_workers, error_threshold, energy_threshold, spatial_adaptivity, filename_expansion, filename_restart_file, filename_model, boundaries_coordinates, minimum_coordinates, Verbose, max_order, DeltaFitting, IsThisARestart, LagrangeFlag, b_type, SamplingMode, sampled_points)
    print('--- ENDED ---')   
    
exit()
